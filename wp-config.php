<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'evo_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '303gento!');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0c:v<8Pjcd3VQ=K[<%/?=7?0Ew?(rFXZ$X_?A~U?9&o_abg=8OKB!VY?yGY:xlWQ');
define('SECURE_AUTH_KEY',  '_6zpV,,{7#]7IoyS-,L+;V:n%H~|cN1a];y6.Deq@pgMw {7t;63z#TsRN{sat~+');
define('LOGGED_IN_KEY',    '+Tt$%_Cq__Y-C3!J2>%-chS 6W+~EIu|*hylKpP|+$Cjz_|^z9loQVVf*&P_ozuS');
define('NONCE_KEY',        'aGEf{2O+XwK(Uj@5awSru= _d1BDODN%<w4of2PR{rAfYAb?B+I_`}%4CW0@Hx-m');
define('AUTH_SALT',        'spINDe;;6D_TRMi]N*64c<{wX>RDLO|=()j%.Vtif_`PR/;c2|&xX=k_5eS.Pzs^');
define('SECURE_AUTH_SALT', '=$Gg3:D)~aCvDD:%>Fe|ntp(1[O&fs|5^0rw>JN|h2u 9Qh#nfHpJM.bwcI?tDDy');
define('LOGGED_IN_SALT',   '*#,(-L`^?5r|7(l`PpP+vzwj|h/V|-RK->O=*-=V}=aDCAR1$jE39lNpv:X[vrXG');
define('NONCE_SALT',       '|{7?HqM$p}V{Oz%+ 2|-Bo-8wfH8 z]XTn}6EQ%-XL{?UIjLQq 8jC/L<sv#M Y+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
