<?php /*** The loop that displays posts.***/ ?>

<?php $al_options = get_option('al_general_settings');?>
<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php _e( 'Not Found', 'Bellezza' ); ?></h1>
		<div class="entry-content">
		<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'Bellezza' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
<?php endif; ?>

<?php if($al_options['al_pagination_pos'] == '1'): ?>
	<?php 
		if ( $wp_query->max_num_pages > 1 )
		{
			include(Bellezza_PLUGINS . '/wp-pagenavi.php' );
			wp_pagenavi(); 
			echo '<div class="clearsmall"></div>';
		}
	?>
<?php endif?> 
 
<?php 
	$postcounter = 1; 
	$attachedImages = array();
	
	while ( have_posts() ) : the_post();
	
	$argsThumb = array(
		'order'          => 'ASC',
		'post_type'      => 'attachment',
		'post_parent'    => $post->ID,
		'post_mime_type' => 'image',
		'post_status'    => null,
		'exclude' => get_post_thumbnail_id()
	);
	$video =  get_post_custom_values("_post_video");
	$type = get_post_custom_values("_post_listing_type");
	if ($type) $type = $type[0];
	$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
?>
	
	<!--Blog Post-->
	<article <?php post_class('blogpost'); ?> id="post-<?php the_ID();?>">
		<?php if ($type == 3): ?>
			 <div class="hover_video">
				<a href="http://vimeo.com/<?php echo $video[0]?>?width=800&amp;height=450" data-rel="prettyPhoto[blogvideo<?php the_ID();?>]" title="<?php the_title()?>">
					<?php the_post_thumbnail('blog-list2', array('class'=>'')); ?>
					<span></span>
				</a>
			</div>
			
		<?php elseif($type==2):?>
			<div class="hover_slideshow">
				<a href="<?php echo $full_image[0]; ?>" title="<?php the_title()?>" data-rel="prettyPhoto[blog<?php the_ID();?>]">
					<?php the_post_thumbnail('blog-list2', array('class'=>'')); ?>
					<span></span>
				</a>
				<?php 
					$attachments = get_posts($argsThumb);
					if ($attachments) {
						foreach ($attachments as $attachment) {
							//echo apply_filters('the_title', $attachment->post_title);
							echo '<a href="'.wp_get_attachment_url($attachment->ID, 'thumbnail', false, false).'" data-rel="prettyPhoto[blog'.$post->ID.']" title="'.get_the_title($post->ID).'"></a>';
						}
					}
				?>
			</div>
		<?php else: ?>
			<div class="hover_link">
				<a href="<?php the_permalink() ?>" title="<?php the_title()?>">
					<?php the_post_thumbnail('blog-list2', array('class'=>'')); ?>
					<span></span>
				</a>
			</div>
		<?php endif ?>
        <div class="blog_box">
			<div class="grid_2">
				<!-- Show Date -->
				<?php if($al_options['al_blog_show_date']): ?>
					<h4><?php the_time('M jS, Y'); ?></h4>
				<?php endif ?>
                <div class="dividerslim"></div>
                <!-- Show Comments amount -->
				<?php if( 'open' == $post->comment_status && $al_options['al_blog_show_comments']) : ?>
					<div class="comment-listing">
					<h6>
						<?php _e('Comments: ', 'Bellezza') ?><?php comments_popup_link('0', '1', '%'); ?>
					</h6>
					</div>
				<?php endif; ?>
				
                 <!-- Show Categories -->
				<?php if($al_options['al_blog_show_cats']): ?>
					<h6>
						<?php if ( count( get_the_category() ) ) : ?>
							<?php $category = get_the_category();  if($category[0]):?>
								<?php _e('Category: ', 'Bellezza') ?><a href="<?php echo get_category_link($category[0]->term_id )?>" class="color"><?php echo $category[0]->cat_name?></a>
							<?php endif?>
						<?php endif; ?>
					</h6>
				<?php endif?>
				
				<div class="dividerslim"></div>
                <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %1$s', 'Bellezza' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark" class="button round">
					<?php echo isset($al_options['al_blogreadmore']) ? $al_options['al_blogreadmore'] : _e ('Post', 'Bellezza') ?>
				</a>
            </div>
			<div class="grid_6">
				<h3>
					<a href="<?php the_permalink(); ?>" class="post-title"><?php the_title()?></a>
				</h3>
				<?php the_excerpt() ?>
			</div>
			
			<div class="clearnospacing"></div>                              
    	</div>
    	<!--End Blog Post-->
    </article>    
    <div class="clear"></div>
	
<?php $postcounter++; endwhile; // End the loop. Whew. ?>
<?php if($al_options['al_pagination_pos'] == '2'): ?>
	<?php 
		if ( $wp_query->max_num_pages > 1 )
		{
			include(Bellezza_PLUGINS . '/wp-pagenavi.php' );
			wp_pagenavi(); 
			echo '<div class="clearsmall"></div>';
		}
	?>
<?php endif?> 
