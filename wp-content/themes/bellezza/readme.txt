Refer to documentation folder on download package for theme documentation.

Version 1.1 Changes:

Fixed the bug after Wordpress 3.6 update
Fixed twitter widget issue
Updated Font awesome to version 3.2.1, 100 more font icons available now
Minor improvements to the code
Optimized Google fonts inclusion
Updated the documentation