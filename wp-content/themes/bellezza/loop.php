<?php /*** The loop that displays posts.***/ ?>

<?php $al_options = get_option('al_general_settings');?>
<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php _e( 'Not Found', 'Bellezza' ); ?></h1>
		<div class="entry-content">
		<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'Bellezza' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
<?php endif; ?>

<div class="container_12">
<?php if($al_options['al_pagination_pos'] == '1'): ?>
	<?php 
		if ( $wp_query->max_num_pages > 1 )
		{
			include(Bellezza_PLUGINS . '/wp-pagenavi.php' );
			wp_pagenavi(); 
			echo '<div class="clearsmall"></div>';
		}
	?>
<?php endif?> 
</div>

<div class="container_12" id="masonry-grid">    	
	<?php 
		$postcounter = 1; 
		$attachedImages = array();
		if ( have_posts() ):
			while ( have_posts() ) : the_post();
			
			$argsThumb = array(
				'order'          => 'ASC',
				'post_type'      => 'attachment',
				'post_parent'    => $post->ID,
				'post_mime_type' => 'image',
				'post_status'    => null,
				'exclude' => get_post_thumbnail_id()
			);
			$video =  get_post_custom_values("_post_video");
			$type = get_post_custom_values("_post_listing_type");
			if ($type) $type = $type[0];
			$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
			?>
			
			<article <?php post_class('blogpost grid_6 masonry-item'); ?> id="post-<?php the_ID();?>">
				<div class="grid_6 omega">
					<div class="projectdetailssm">                                
						<div class="grid_6 omega"> 
							<?php if ($type == 3): ?>
								<div class="hover_video masonry-hover">
									<a href="http://vimeo.com/<?php echo $video[0]?>?width=430&amp;height=200" data-rel="prettyPhoto[blogvideo<?php the_ID();?>]" title="<?php the_title()?>">
										<?php the_post_thumbnail('blog-list', array('class'=>'')); ?>
										<span></span>
									</a>
								</div>
								
							<?php elseif($type==2):?>
								<div class="hover_slideshow masonry-hover">
									<a href="<?php echo $full_image[0]; ?>" title="<?php the_title()?>" data-rel="prettyPhoto[blog<?php the_ID();?>]">
										<?php the_post_thumbnail('blog-list', array('class'=>'')); ?>
										<span></span>
									</a>
									<?php 
										$attachments = get_posts($argsThumb);
										
										if ($attachments) {
											foreach ($attachments as $attachment) {
												//echo apply_filters('the_title', $attachment->post_title);
												echo '<a href="'.wp_get_attachment_url($attachment->ID, 'thumbnail', false, false).'" data-rel="prettyPhoto[blog'.$post->ID.']" title="'.get_the_title($post->ID).'"></a>';
												
											}
										}
									?>
								</div>
							<?php else: ?>
								<div class="hover_link masonry-hover">
									<a href="<?php the_permalink() ?>" title="<?php the_title()?>">
										<?php the_post_thumbnail('blog-list', array('class'=>'')); ?>
										<span></span>
									</a>
								</div>
							<?php endif ?>
							
							
							<!--Date-->
							<div class="grid_2_no_margin top20">
								<?php if($al_options['al_blog_show_date']): ?>
									   <h2><?php the_time('M jS'); ?></h2>
									   <h3><?php the_time('Y'); ?></h3>
									   <div class="clearextrasmall"></div>
								<?php endif?>
								<h5>
									<?php if($al_options['al_blog_show_author']): ?>
										<?php printf( __( 'By <span class="color">%1$s</span>', 'Bellezza' ),  get_the_author()) ?><br />
									<?php endif?>
									<?php if($al_options['al_blog_show_cats']): ?>
										<?php if ( count( get_the_category() ) ) : ?>
											<?php $category = get_the_category();  if($category[0]):?>
												<?php _e('Category: ', 'Bellezza') ?><a class="color" href="<?php echo get_category_link($category[0]->term_id )?>"><?php echo $category[0]->cat_name?></a><br />
											<?php endif?>
										<?php endif; ?>
									<?php endif?>
									<?php if( 'open' == $post->comment_status && $al_options['al_blog_show_comments']) : ?>
										<span class="comment-listing"><?php _e('Comments ', 'Bellezza') ?><?php comments_popup_link('0', '1', '%'); ?></span>
									<?php endif; ?>
								</h5>
							</div>
							<div class="grid_4 blog-article-desc">         
								<!--Title-->
								<h4><?php the_title(); ?></h4>                                                     
								<!--Description-->
								<div><?php the_excerpt(); ?></div> 
								<div>
									<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %1$s', 'Bellezza' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark" class="button highlight small">
										<?php echo isset($al_options['al_blogreadmore']) ? $al_options['al_blogreadmore'] : _e ('Read More', 'Bellezza') ?>
									</a>
								</div>                
							</div>
							<div class="clearnospacing"></div>
						</div>
						<div class="clearnospacing"></div>
					</div>
				</div>
				<div class="clearnospacing"></div>
			</article>
			<!--End Blog Post-->
			
		<?php  endwhile; // End the loop. Whew. ?>
	<?php endif?>
</div>		
			
<div class="clearsmall"></div>
<div class="container_12">
	<?php if($al_options['al_pagination_pos'] == '2'): ?>
		<?php 
			if ( $wp_query->max_num_pages > 1 )
			{
				include(Bellezza_PLUGINS . '/wp-pagenavi.php' );
				wp_pagenavi(); 
				echo '<div class="clearsmall"></div>';
			}
		?>
	<?php endif?> 
</div>

<div class="clearsmall"></div>

