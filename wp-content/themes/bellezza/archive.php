<?php
/*** The template for displaying Archive pages. **/

get_header(); ?>


<?php $promo = get_post_meta($post->ID, "_promo", $single = false);?>

<!-- Title and promo text -->
<div class="box">
	<!-- Title -->
	<div class="headertext">
		<?php if ( is_day() ) : ?>
			<?php printf( __( 'Daily Archives: <span>%s</span>', 'Bellezza' ), get_the_date() ); ?>
		<?php elseif ( is_month() ) : ?>
			<?php printf( __( 'Monthly Archives: <span>%s</span>', 'Bellezza' ), get_the_date('F Y') ); ?>
		<?php elseif ( is_year() ) : ?>
			<?php printf( __( 'Yearly Archives: <span>%s</span>', 'Bellezza' ), get_the_date('Y') ); ?>
		<?php elseif ( is_category() ) : ?>
			<?php single_cat_title();?>
		<?php else : ?>
			<?php _e( 'Blog Archives', 'Bellezza' ); ?>
		<?php endif; ?>
        
	</div>
	<!-- Page Contents -->
	<div class="container_12">
		<div class="grid_9">   	
			<?php
				if ( have_posts() ) the_post();
				rewind_posts();    
				get_template_part( 'loop-default', 'archive' );
			?>
		</div>
		<aside class="grid_3_no_margin sidebarright alignright">
		   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?> <?php   endif;?>
		</aside> 
		<div class="clearfix"></div>
	</div>
</div>

<?php get_footer(); ?>
