<?php
/* ***************************************************** 
 * File Description: Contains all theme's shortcodes
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */


/***************** BUTTONS ********************/

function al_button($atts, $content = null) {

	extract(shortcode_atts(array(
		"id"	=> '',
		"url" 	=> '#', 
		"size"	=> 'medium',
		"type"	=> 'normal',
		"bg"	=> '',
	), $atts));
	
	$id = isset($id) && $id != '' ? 'id="'.$id.'"' : '';
	
	return '<a href="'.$url.'" '.$id.' class="button '.$size.' '.$type.' '.$bg.'">'.$content.'</a>';
	
}
add_shortcode('button', 'al_button');

/************************************************/



/*********** SINGLE ARTICLE BY ID ***************/

function sc_article($atts, $content = null) {

	extract(shortcode_atts(array(
		"id"=> '',
	), $atts));
	
	$post = get_post($id);
	$url = get_permalink($id);
	return '<h2><a href="'.$url.'">'.$post->post_title.'</a></h2><div>'.do_shortcode($post->post_content).'</div>';
	
}
add_shortcode('article', 'sc_article');

/************************************************/


/*********** MEMBER INFO ***************/

function al_member($atts, $content = null) {

	extract(shortcode_atts(array(
		"photo"=> '',
		"name"=> '',
		"position" => '',
		"email" => ''
		
	), $atts));
	
	$return = '
	<div class="grid_3 nomargin">                
		<div class="alignleft"><img src="'.$photo.'" alt=""/></div>	
		<h4>'.$name.'<br /><span>'.$position.'</span></h4>            
		<p><a href="mailto:'.$email.'">'.$email.'</a></p>
	</div>';
	

	return $return;
	
}
add_shortcode('member', 'al_member');

/************************************************/



/*************** SOCIAL BUTTONS *****************/

function al_socialbutton($atts, $content = null) {

	extract(shortcode_atts(array(
		"name"	=> '',
		"url" 	=> '#', 
		"icon"	=> '',
		"title"	=> '',
		"target" => '_blank',
		"fade" => '0',
		"class" => '' 
	), $atts));
	
	$fade = isset($fade) && $fade != '0' ? 'fade' : '';
	
	$title = isset($title) && $title != '' ? $title : $name;
	$predefined = array('facebook', 'twitter', 'rss', 'youtube', 'mail', 'flickr', 'linkedin', 'vimeo', 'skype', 'myspace');
	$icon = in_array($name, $predefined) ? get_template_directory_uri().'/images/social_icons/'.$name.'.png' : $icon.'' ;
	
	return '<a href="'.$url.'" class="'.$class.'" target="'.$target.'"><img class="social '.$fade.'" src="'.$icon.'" alt="'.$title.'" /><span class="tip">'.$title.'</span></a>';
}
add_shortcode('social_button', 'al_socialbutton');

/************************************************/


/************** TEXT HIGLIGHTING ****************/

function al_highlight($atts, $content = null) {
	return '<span class="highlight">'.$content.'</span>';
}
add_shortcode('highlight', 'al_highlight');

/************************************************/


/****** SHOW POSTS BY CATEGORY AND COUNT ********/

function al_list_posts( $atts )
{

	extract( shortcode_atts( array(
		'title' => 'Latest from the blog',
		'url' => '',
		'urlcaption' => 'View all posts',
		'category' => '',
		'limit' => '5',
		'order' => 'DESC',
		'orderby' => 'date',
		'class' =>''
	), $atts) );

	$return = '';

	$query = array();

	if ( $category != '' )
		$query[] = 'category=' . $category;

	if ( $limit )
		$query[] = 'numberposts=' . $limit;

	if ( $order )
		$query[] = 'order=' . $order;

	if ( $orderby )
		$query[] = 'orderby=' . $orderby;

	$posts_to_show = get_posts( implode( '&', $query ) );

	$return = '
	
	<h3 class="uppercase">'.$title;
	if ($url)
	{
		$return.='<span class="lowercase"><a href="'.$url.'" class="button minimal small alignrightnopad">'.$urlcaption.'</a></span>';
	}
	$return.='
	</h3>
	<div class="clearsmall"></div>
	<div class="wall sticktoedge">
		<ul class="'.$class.'">';
			foreach ($posts_to_show as $ps) 
			{
				$return .='
				<li>
					<a href="'.get_permalink( $ps->ID ).'" class="alternate2" title="'.$ps->post_title.'">
						'.get_the_post_thumbnail($ps->ID, 'portfolio-wall').'
						<strong>'.$ps->post_title.'</strong>
						<span></span>
					</a>	
				</li>';
			}
			$return .= '
		</ul>
	</div>';
    return $return;
}

add_shortcode('list_posts', 'al_list_posts');


/*************** RELATED POSTS ******************/

function al_related_posts( $atts ) {
	extract(shortcode_atts(array(
	    'limit' => '5',
	    
	), $atts));

	global $wpdb, $post, $table_prefix;

	if ($post->ID) {
		$retval = '<ul>';
 		// Get tags
		$tags = wp_get_post_tags($post->ID);
		$tagsarray = array();
		foreach ($tags as $tag) {
			$tagsarray[] = $tag->term_id;
		}
		$tagslist = implode(',', $tagsarray);

		// Do the query
		$q = "SELECT p.*, count(tr.object_id) as count
			FROM $wpdb->term_taxonomy AS tt, $wpdb->term_relationships AS tr, $wpdb->posts AS p WHERE tt.taxonomy ='post_tag' AND tt.term_taxonomy_id = tr.term_taxonomy_id AND tr.object_id  = p.ID AND tt.term_id IN ($tagslist) AND p.ID != $post->ID
				AND p.post_status = 'publish'
				AND p.post_date_gmt < NOW()
 			GROUP BY tr.object_id
			ORDER BY count DESC, p.post_date_gmt DESC
			LIMIT $limit;";

		$related = $wpdb->get_results($q);
 		if ( $related ) {
			foreach($related as $r) {
				$retval .= '<li><a title="'.wptexturize($r->post_title).'" href="'.get_permalink($r->ID).'">'.wptexturize($r->post_title).'</a></li>';
			}
		} else {
			$retval .= '
	<li>No related posts found</li>';
		}
		$retval .= '</ul>';
		return $retval;
	}
	return;
}
add_shortcode('related_posts', 'al_related_posts');

/************************************************/


/***************** LIST PAGES *******************/

function al_list_pages($atts, $content, $tag) {
	global $post;
		
	// set defaults
	$defaults = array(
	    'class'       => $tag,
	    'depth'       => 0,
	    'show_date'   => '',
	    'date_format' => get_option('date_format'),
	    'exclude'     => '',
	    'child_of'    => 0,
	    'title_li'    => '',
	    'authors'     => '',
	    'sort_column' => 'menu_order, post_title',
	    'link_before' => '',
	    'link_after'  => '',
	    'exclude_tree'=> ''
	);
	
	
	$atts = shortcode_atts($defaults, $atts);
	
	
	$atts['echo'] = 0;
	if($tag == 'child-pages')
		$atts['child_of'] = $post->ID;	

	// create output
	$out = wp_list_pages($atts);
	if(!empty($out))
		$out = '<ul class="'.$atts['class'].'">' . $out . '</ul>';
	
  return $out;
}

add_shortcode('child-pages', 'al_list_pages');
add_shortcode('list-pages', 'al_list_pages');

/************************************************/


/*************** DIVIDER LINE ******************/

function al_divider($atts, $content = null) {
	  extract(shortcode_atts(array(
        'top' => '',       
    ), $atts));
	  
	if ($top)
	{
		return '<div class="divider"><h5><a class="alignright toTop" href="#">Top &uarr;</a></h5></div>';
	}
	else
	{
   		return '<div class="divider"></div>';
	}
}
add_shortcode('divider', 'al_divider');

/***********************************************/


/*************** STEP BLOCK ******************/

function al_stepbox($atts, $content = null) {
	  extract(shortcode_atts(array(
        'step' => 'one', 
		'title' => ''		
    ), $atts));
	
	$return = '
		<div class="showbox nomargin boxpadding stepbox">
			<h3 class="uppercase">
				<span class="step">'.$step.'</span>'.$title.'
			</h3>       
			'.do_shortcode($content).'
        </div>';	
	
   	return $return;
}
add_shortcode('stepbox', 'al_stepbox');

/***********************************************/


/*************** DARK BLOCK ******************/

function al_darkbox($atts, $content = null) { 
	return '<div class="newsletterbar sticktoedge darkbox">'.do_shortcode($content).'</div>';		
} 
add_shortcode('darkbox', 'al_darkbox');

/***********************************************/


/************** CALLOUT BLOCK ******************/

function al_callout($atts, $content = null) {
	  extract(shortcode_atts(array(
      	'title' => ''		
    ), $atts));
	
	$return = '
		<div class="calloutcontainer sticktoedge">
		   <div class="text">'.$title.'</div>
		   '.do_shortcode($content).'
		</div>';
	
   	return $return;
}
add_shortcode('callout', 'al_callout');

/***********************************************/




/*************** CONTACT DETAILS ***************/

function al_contact_info($atts, $content = null) {
	  extract(shortcode_atts(array(
		'type' => '1',
        'phone' => '', 
		'fax' => '',
		'email' => '',
		'address' => ''			
    ), $atts));
	
	$return = '';
	
	if ($type == 1)
	{
	
		if ($phone)
		{
			$return .= '<p class="highlight"><span class="phone">'.$phone.'</span></p>';	
		}	
		if ($fax)
		{
			$return .= '<p class="highlight"><span class="fax">'.$fax.'</span></p>';	
		}	
		
		if ($email)
		{
			$return .= '<p class="highlight"><span class="email bold"><a href="mailto:'.$email.'">'.$email.'</a></span></p>';	
		}	
		if ($address)
		{
			$return .= '<p class="highlight"><span class="mail">'.$address.'</span></p>';	
		}		
	}
	else
	{
		if ($phone)
		{
			$return .= '<p><span class="phonecontact">'.$phone.'</span></p>';	
		}	
		if ($fax)
		{
			$return .= '<p><span class="faxcontact">'.$fax.'</span></p>';	
		}	
		
		if ($email)
		{
			$return .= '<p><span class="emailcontact"><a href="mailto:'.$email.'">'.$email.'</a></span></p>';	
		}	
		if ($address)
		{
			$return .= '<p><span class="mailcontact">'.$address.'</span></p>';	
		}		
	}
	
   	return $return;
}
add_shortcode('contact_info', 'al_contact_info');

/***********************************************/



/****************** SPACING ********************/

function al_spacing($atts, $content = null) {
  extract(shortcode_atts(array(
        'type' => 'top',
        'amount' => '10',
   ), $atts));
   return '<div class="'.$type.$amount.'"></div>';
}
add_shortcode('spacing', 'al_spacing');

/************************************************/


/************* VIMEO EMBED (VIA ID) *************/

function al_vimeo($atts, $content=null) {
	
	   extract(shortcode_atts(array(
            "id" => '',
            "width" => '621',
            "height" => '350',
			"class" => ''
        ), $atts));
    
    /*$data = '
		<object class="'.$class.'" width="'.$width.'" height="'.$height.'" data="http://vimeo.com/moogaloop.swf?clip_id='.$id.'&amp;server=vimeo.com" type="application/x-shockwave-flash">
            <param name="allowfullscreen" value="true" />
			<param name="wmode" value="transparent" />
            <param name="allowscriptaccess" value="always" />
            <param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id='.$id.'&amp;server=vimeo.com" />
        </object>';*/
	$data = '<iframe src="http://player.vimeo.com/video/'.$id.'?title=0&amp;byline=0&amp;portrait=0&amp;color=00adef" width="'.$width.'" height="'.$height.'" class="'.$class.'"></iframe>';
	
    return $data;
}
add_shortcode('vimeo', 'al_vimeo');

/************************************************/


/************** YOUTUBE EMBED (VIA ID) **********/ 
$youtube_nr = 0;
function al_youtube($atts, $content=null) {
	 extract(shortcode_atts(array(
			'id'  => '',
			'width'  => '600',
			'height' => '340',
			"class" => 'frame'
			), $atts));

		return '<div class="youtube_video"><object class="'.$class.'" type="application/x-shockwave-flash" style="width:'.$width.'px; height:'.$height.'px;" data="http://www.youtube.com/v/'.$id.'&amp;hl=en_US&amp;fs=1&amp;"><param name="movie" value="http://www.youtube.com/v/'.$id.'&amp;hl=en_US&amp;fs=1&amp;" /></object></div>';

}
add_shortcode('youtube', 'al_youtube');
/************************************************/


/***************** CLEAR ************************/

function al_clear($atts, $content = null) {	
	 extract(shortcode_atts(array(
        "type" => '',            
    ), $atts));
	return '<div class="clear'.$type.'"></div>';
}
add_shortcode('clear', 'al_clear');

/************************************************/


/********* STANDARD UNORDERED LISTS *************/

function al_list($atts, $content = null) {
	extract(shortcode_atts(array(
		'type' 	=> '',	
	), $atts));

	return '<div class="list'.$type.'">'.$content.'</div>';
}
add_shortcode('list', 'al_list');

/************************************************/


/****************** COLUMNS *********************/

// COLUMN FULL
function full( $atts, $content = null ) {
   return '<div class="grid_12">' . do_shortcode($content) . '</div>'; }
add_shortcode('full', 'full');
// COLUMN 1/2
function one_half( $atts, $content = null ) {
   return '<div class="halfwidth">' . do_shortcode($content) . '</div>'; }
add_shortcode('one_half', 'one_half');

// COLUMN 1/2 Last
function one_half_last( $atts, $content = null ) {
   return '<div class="halfwidth omega">' . do_shortcode($content) . '</div><div class="clearnospacing"></div>'; }
add_shortcode('one_half_last', 'one_half_last');

// COLUMN 1/3 
function one_third( $atts, $content = null ) {
   return '<div class="thirdwidth">' . do_shortcode($content) . '</div>'; }
add_shortcode('one_third', 'one_third');

// COLUMN 1/3 Last
function one_third_last( $atts, $content = null ) {
   return '<div class="thirdwidth omega">' . do_shortcode($content) . '</div><div class="clearnospacing"></div>'; }
add_shortcode('one_third_last', 'one_third_last');

// COLUMN 1/4
function one_fourth( $atts, $content = null ) {
   return '<div class="fourthwidth">' . do_shortcode($content) . '</div>'; }
add_shortcode('one_fourth', 'one_fourth');

// COLUMN 1/4 Last
function one_fourth_last( $atts, $content = null ) {
   return '<div class="fourthwidth omega">' . do_shortcode($content) . '</div><div class="clearnospacing"></div>'; }
add_shortcode('one_fourth_last', 'one_fourth_last');

// COLUMN 1/6
function one_sixth( $atts, $content = null ) {
   return '<div class="sixthwidth">' . do_shortcode($content) . '</div>'; }
add_shortcode('one_sixth', 'one_sixth');

// COLUMN 1/6 Last
function one_sixth_last( $atts, $content = null ) {
   return '<div class="sixthwidth omega">' . do_shortcode($content) . '</div><div class="clearnospacing"></div>'; }
add_shortcode('one_sixth_last', 'one_sixth_last');

/************************************************/


/******************* QUOTES *********************/

function al_quote( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'author' => '',
		'type'	 => '',
	), $atts));
	$return = '';
	if ($type == 'static')
	{
		$return = '<div class="quote"><div class="static">'.do_shortcode($content).'<br /><span>'.$author.'</span></div></div>';
	}
	else
	{
		$return = '<div class="blockquote">'.do_shortcode($content).'<br /><span>'.$author.'</span></div>';
	}
	$return.= '<div class="clearnospacing"></div>';
	return $return; 
}
add_shortcode('quote', 'al_quote');

/************************************************/


/*******************  TABS  *********************/

add_shortcode( 'tabgroup', 'al_tab_group' );
function al_tab_group( $atts, $content ){
	extract(shortcode_atts(array(
		'location' => ''
	), $atts));
	$GLOBALS['tab_count'] = 0;
	$location = empty($location) ? '' : ' '.$location.'tab';
	do_shortcode( $content );
	
	if( is_array( $GLOBALS['tabs'] ) ){
	foreach( $GLOBALS['tabs'] as $tab ){
	$tabs[] = '<li><a class="" href="#">'.$tab['title'].'</a></li>';
	$panes[] = '<div class="pane'.$location.'">'.do_shortcode($tab['content']).'</div>';
	}
	$return = "\n".'<div class="wrap"><ul class="tabs'.$location.'">'.implode( "\n", $tabs ).'</ul><div class="clearnospacing"></div>'."\n".'<div class="panes clearfix">'.implode( "\n", $panes ).'</div></div>'."\n";
	}
	return $return;
}

add_shortcode( 'tab', 'al_tab' );

function al_tab( $atts, $content ){
	extract(shortcode_atts(array(
	'title' => 'Tab %d'
	), $atts));
	
	$x = $GLOBALS['tab_count'];
	$GLOBALS['tabs'][$x] = array( 'title' => sprintf( $title, $GLOBALS['tab_count'] ), 'content' =>  $content );
	
	$GLOBALS['tab_count']++;
}

/************************************************/


/************ TESTIMONIALS ROTATOR **************/

add_shortcode( 'trotator', 'al_trotator' );
function al_trotator( $atts, $content ){
	extract(shortcode_atts(array(
		'interval' => '4000'
	), $atts));
	$count = 0;
	$GLOBALS['tritem_count']=0;
	do_shortcode( $content );

	if( is_array( $GLOBALS['tritem'] ) ){
		foreach( $GLOBALS['tritem'] as $tab ){
			if ($count == 0)
			{
				$panes[] = '<li class="slide">'.do_shortcode($tab['content']).'<span> <br />'.$tab['author'].'</span></li>';	
			}
			else
			{
				$panes[] = '<li class="slide" style="display:none">'.do_shortcode($tab['content']).'<span>'.$tab['author'].'</span></li>';		
			}
			$count++;
		}
		$return = "\n".'<ul id="testimonials">'.implode( "\n", $panes ).''."\n".'</ul>';
		$return.='<script type="text/javascript">
		jQuery(document).ready(function(){
		jQuery(\'#testimonials .slide\');
		setInterval(function(){
			jQuery(\'#testimonials .slide\').filter(\':visible\').fadeOut(2000,function(){
				if(jQuery(this).next().size()){jQuery(this).next().fadeIn(1000);}
				else{jQuery(\'#testimonials .slide\').eq(0).fadeIn(1000);}
			});
		},'.$interval.'); });
		</script>';
	}
	return $return;
}

add_shortcode( 'tritem', 'al_tritem' );

function al_tritem( $atts, $content ){
	extract(shortcode_atts(array(
	'author' => ''
	), $atts));
	
	$x = $GLOBALS['tritem_count'];
	$GLOBALS['tritem'][$x] = array('author' => $author, 'content' =>  $content );
	
	$GLOBALS['tritem_count']++;
}

/************************************************/



/***************  SLIDERS  ******************/

add_shortcode('slider', 'al_slider' );
function al_slider( $atts, $content ){
	$GLOBALS['slideritem_count'] = 0;
	extract(shortcode_atts(array(
		'width' => '660',
		'height' => '240',
		'interval' => '4000',
		'nav' => 'false' 
	), $atts));
	do_shortcode( $content );
		
	if( is_array( $GLOBALS['sitems'] ) ){
		$icount = 0;
		foreach( $GLOBALS['sitems'] as $item ){
			if (isset($item['image']) && $item['image'] !=='')
			{
				$panes[] = '<li><img src="'.$item['image'].'" alt="'.$item['title'].'"/></li>';  
			}
			else
			{
				$panes[] = '<li>'.do_shortcode($item['content']).'</li>'; 
			}			
			$icount ++ ;
		}
		
		$randomId = mt_rand(0, 100000);
		$return ='
		<div class="basic-slider-shortcode" style="width:'.$width.'px">
			<div id="basic-slider-'.$randomId.'">
				<ul class="bjqs">'.implode( "\n", $panes ).'</ul>
			</div>
		</div>
		<div class="clearnospacing"></div>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery("#basic-slider-'.$randomId.'").bjqs({
					width : '.$width.',
					height : '.$height.',
					rotationSpeed: '.$interval.',
					showControls: true,
					centerControls: true,
					nextText: "",
					prevText: "",
					showMarkers: '.$nav.',
					centerMarkers: true,
					keyboardNav: true,
					useCaptions: true
				});
			});
		</script>';	
	}
	return $return;
}


add_shortcode( 'slideritem', 'al_slideritem' );

function al_slideritem( $atts, $content ){
	extract(shortcode_atts(array(
		'image' => '',
		'title' => '',
	), $atts));
	
	$x = $GLOBALS['slideritem_count'];
	$GLOBALS['sitems'][$x] = array( 'image' => $image, 'title' => $title, 'content' =>  $content );
	
	$GLOBALS['slideritem_count']++;
	
}

/*-- Nivo Slider --*/

add_shortcode('nivo', 'al_nivo' );
function al_nivo( $atts, $content ){
	wp_enqueue_style('nivo-project-styles');
	$GLOBALS['nivoitem_count'] = 0;
	extract(shortcode_atts(array(
		'interval' => '4000',
		'width' => '670',
		'height' => '240',
	), $atts));
	do_shortcode( $content );
		
	if( is_array( $GLOBALS['nivoitems'] ) ){
		$icount = 1;
		foreach( $GLOBALS['nivoitems'] as $item ){
			$panes[] = '<img src="'.$item['image'].'" alt=""  title="#htmlcaption'.$icount.'" />';   		
			$icount ++ ;
		}
		
		//$randomId = mt_rand(0, 100000);
		$icount2 = 1;
		$return='
		<div class="nivo_container" style="margin-left:-40px">
            <div id="slider" class="nivoSlider" style="width:'.$width.'px; height:'.$height.'px">
				'.implode( "\n", $panes ).'
			</div>';
			
			foreach( $GLOBALS['nivoitems'] as $item ){
				$title = (isset($item['title']) && $item['title']!=='') ? '<h3>'.$item['title'].'</h3>' : ''; 
				$return.='<div id="htmlcaption'.$icount2.'" class="nivo-html-caption">'.$title.$item['content'].'</div>';
				$icount2 ++ ;
			}
			
			$return.='
			<div class="clearnospacing"></div>
		</div>
		<script type="text/javascript">
			jQuery(window).load(function() {
				jQuery("#slider").nivoSlider({pauseTime:'.$interval.'});
			});
		</script>';	
	}
	return $return;
}


add_shortcode( 'nivoitem', 'al_nivoitem' );

function al_nivoitem( $atts, $content ){
	extract(shortcode_atts(array(
		'image' => '',
		'title' => ''
	), $atts));
	
	$x = $GLOBALS['nivoitem_count'];
	$GLOBALS['nivoitems'][$x] = array( 'image' => $image, 'title'=>$title, 'content' =>  do_shortcode ($content) );
	
	$GLOBALS['nivoitem_count']++;
	
}


/************************************************/


/***************** TOGGLES **********************/

add_shortcode( 'togglegroup', 'al_togglegroup' );
function al_togglegroup( $atts, $content ){
	extract(shortcode_atts(array(
		'class' => '',
	), $atts));
	$GLOBALS['toggle_count'] = 0;
	do_shortcode( $content );
	if( is_array( $GLOBALS['toggles'] ) ){
	foreach( $GLOBALS['toggles'] as $toggle ){
		$panes[] = '<div class="toggle-trigger"><a href="#" class="tr'.$class.'">'.$toggle['title'].'</a></div> <div class="toggle-container"><div class="toggle-block">'.do_shortcode($toggle['content']).'</div></div>'; 
	}
	$return = '<div id="wrapper"><div class="demo">'.implode( "\n", $panes ).'</div></div>';	
	}
	return $return; 
}

add_shortcode( 'toggle', 'al_toggle' );

function al_toggle( $atts, $content ){
	extract(shortcode_atts(array(
	'title' => '',	
	), $atts));
	
	$x = $GLOBALS['toggle_count'];
	$GLOBALS['toggles'][$x] = array('title' => $title, 'content' =>  $content);	
	$GLOBALS['toggle_count']++;
}

/************************************************/


/***************** Lightbox *********************/

add_shortcode( 'lightboxgroup', 'al_lightboxgroup' );
function al_lightboxgroup( $atts, $content ){
	$GLOBALS['lightbox_count'] = 0;
	do_shortcode( $content );
	if( is_array( $GLOBALS['lightboxs'] ) ){
	foreach( $GLOBALS['lightboxs'] as $lightbox ){
		$type = $lightbox['type'];
		$add = '';
		$link = '';
		if ($type == 'image' || $type == 'video')
		{
			$add = 'data-rel="prettyPhoto"';
			$link = $lightbox['full'];
		}
		elseif ($type=='slideshow')
		{
			$add = 'data-rel="prettyPhoto[gallery1]"';
			$link = $lightbox['full'];
		}
		elseif ($type=='link')
		{
			$link = $lightbox['url'];
		}
		
		elseif ($type=='mail')
		{
			$link = 'mailto:'.$lightbox['url'];
		}
		
		$panes[] = 
		'<li>
			<div class="hover_'.$lightbox['type'].'">
				<a title="'.$lightbox['title'].'" href="'.$link.' '.$add.'">
					<img  alt="'.$lightbox['title'].'" src="'.$lightbox['thumb'].'">
					<span></span>
				</a>
			</div>
			<div class="text_box">
				<h4>'.$lightbox['title'].'</h4>
				<p>'.do_shortcode($lightbox['content']).'</p>
			</div>
		</li>';
	}
	$return = '<ul id="list" class="image-grid threecol portfolio-content">'.implode( "\n", $panes ).'</ul>';	
	}
	return $return;
}

function al_lightbox( $atts, $content = null)
{
   extract(shortcode_atts(array(
		'thumb'	=> '',
		'full'	=> '',
		'title' => '',
		'type' => 'image',
		'url' => ''
	), $atts));	
	
	$x = $GLOBALS['lightbox_count'];
	$GLOBALS['lightboxs'][$x] = array('title' => $title, 'thumb' => $thumb, 'full' => $full, 'type' => $type, 'url' => $url,  'content' =>  $content);	
	$GLOBALS['lightbox_count']++;
	
}
add_shortcode('lightbox', 'al_lightbox');

/************************************************/


/***************** MESSAGES  ********************/

function al_message($atts, $content = null ) {
	extract(shortcode_atts(array(
		'type'	=> 'success',
		'class'	=> '',
	), $atts));
	
	$messageType = '';
	switch ($type)
	{
		case 'success':
		$messageType = 'greenbox'; 
		break;
		
		case 'info':
		$messageType = 'bluebox'; 
		break;
		
		case 'warning':
		$messageType = 'yellowbox'; 
		break;
		
		case 'error':
		$messageType = 'redbox'; 
		break;
		
	}
	
	return '<div class="'.$class.' '.$messageType.'"><h2>'.$content.'</h2></div>';
	
}
add_shortcode('message', 'al_message');

/************************************************/


/**************** ALIGNED IMAGES ****************/

function al_alimage( $atts, $content = null)
{
   extract(shortcode_atts(array(
		'type'	=> 'left',	
		'width' => '400'
	), $atts));	
   return '<div class="align'.$type.'" style="width:'.$width.'px; margin-'.$type.':20px">'. do_shortcode($content) . '</div>';
}
add_shortcode('align', 'al_alimage');

/************************************************/



/******** SHORTCODE SUPPORT FOR WIDGETS *********/

if (function_exists ('shortcode_unautop')) {
	add_filter ('widget_text', 'shortcode_unautop');
}
add_filter ('widget_text', 'do_shortcode');

/************************************************/


/************* LIST POPULAR POSTS ***************/

function al_popular_posts($atts, $content = null) {
    extract(shortcode_atts(array(
            "limit" => '2'
    ), $atts));

   
	global $post;
    $query = new WP_Query();
	$query->query('ignore_sticky_posts=1&showposts='.$limit.'&orderby=comment_count');
	
	 $retour='<ul class="popular-posts">';
	$i = 1;
	while ($query->have_posts()) : $query->the_post(); 
	
    	$thumb = has_post_thumbnail() ?  get_the_post_thumbnail($post->ID, 'blog-thumb') : '';
		$categories =  get_the_category($post->ID);
		$category = $categories[0]->cat_name;
		  $retour.='
		<li>
			'.$divider.'
			<div class="alignleft">
            	<a href="'.get_permalink($post->ID).'" class="wt-title">'.$thumb.'</a>
			</div>	
            <a href="'.get_permalink($post->ID).'" class="tip_trigger"><img src="'.get_template_directory_uri().'/images/icons/user.png" class="imgspace" alt=""/><span class="tip">'.get_the_author().'</span></a>
            <a href="'.get_permalink($post->ID).'" class="tip_trigger"><img src="'.get_template_directory_uri().'/images/icons/tag.png" class="imgspace" alt=""/><span class="tip">'.$category.'</span></a>
            <a href="'.get_permalink($post->ID).'" class="tip_trigger"><img src="'.get_template_directory_uri().'/images/icons/comment.png" alt=""/><span class="tip">'.do_shortcode('[comments_count id="'.$post->ID.'" /]').'</span></a>            
        	<p>'.limit_words(get_the_excerpt(), '6').'</p>	
        	
			<div class="clearnospacing"></div>
			<div class="dividerblog"></div>
		</li>
		';
		$i++;
        endwhile;
        $retour.='</ul>';
     
                           
        return $retour;
}
add_shortcode("popular_posts", "al_popular_posts");

/************************************************/


/* ****** Display number of comments for specific post ****** */
function al_comments_count($atts) {
	extract( shortcode_atts( array(
		'id' => ''
	), $atts ) );

	$num = 0;
	$post_id = $id;
	$queried_post = get_post($post_id);
	$cc = $queried_post->comment_count;
		if( $cc == $num || $cc > 1 ) : $cc = $cc.' Comments';
		else : $cc = $cc.' Comment';
		endif;
	$permalink = get_permalink($post_id);

	//return '<a href="'. $permalink . '" class="comments_link">' . $cc . '</a>';
	return $cc;
}
add_shortcode('comments_count', 'al_comments_count');


/************* PORTFOLIO WORKS ***************/

function al_list_portfolio($atts, $content = null) {
    extract(shortcode_atts(array(
            "title" => 'FEATURED PROJECTS',
			"url" => '',
			"urlcaption" => 'View our portfolio',
			"limit" => '6',
			"featured" => 1
    ), $atts));
 	global $post;
	$return = '';
    $counter = 1; 
	$args = array('post_type' => 'portfolio', 'taxonomy'=> 'portfolio_category', 'showposts' => $limit, 'posts_per_page' => $limit, 'orderby' => 'date','order' => 'DESC');
	
	if ($featured)
	{
		$args['meta_key'] = '_portfolio_featured'; 
		$args['meta_value'] = '1';
	}
	
   	$query = new WP_Query($args);
	
	$return.='
	<div class="container_12">
		<div class="recent">
			<h3>'.$title;
			if ($url)
			{
				$return.='<span><a href="'.$url.'" class="button minimal small alignrightnopad fade">'.$urlcaption.'</a></span>';
			}
			$return.='</h3>
			<div class="clearsmall"></div>';
						
			while ($query->have_posts())  : $query->the_post(); 
			$custom = get_post_custom($post->ID);  	
			$return.='
			<div class="clients clients-first">
				'.get_the_post_thumbnail($post->ID, 'portfolio-listing', array('class' => 'cover')).'
				<div class="mask">';
					if( !empty ( $custom['_portfolio_video'][0] ) ) :
						$return.= '<a href="'.$custom['_portfolio_video'][0].'" class="video" title="'.get_the_title().'" data-rel="prettyPhoto"></a>';
					elseif( isset($custom['_portfolio_link'][0]) && $custom['_portfolio_link'][0] != '' ) : 
						$return.= '<a href="'.$custom['_portfolio_link'][0].'" class="url" title="'.get_the_title().'"></a>';
					elseif(  isset( $custom['_portfolio_no_lightbox'][0] )  &&  $custom['_portfolio_no_lightbox'][0] !='' ) :
						$return.= '<a href="'.get_permalink().'" class="url" title="'.get_the_title().'"></a>';
					else : 
						$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false); 
						$return.= '<a href="'.$full_image[0].'" class="image" title="'.get_the_title().'"  data-rel="prettyPhoto[\'pp_gal\']"></a>';
					endif; 					
					$return.=
				'</div>
			</div>';
			endwhile; wp_reset_query();
			$return.='
		</div>
		<div class="clear"></div>			
	</div>';
	
	return $return;
	
}
add_shortcode("list_portfolio", "al_list_portfolio"); 
	
?>