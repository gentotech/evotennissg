<?php

/**** WIDGETS AREA ****/


/* ***************************************************** 
 * Plugin Name: Bellezza Flickr
 * Description: Retrieve and display photos from Flickr.
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
class al_flickr_widget extends WP_Widget {

	// Widget setup.
	function al_flickr_widget() {

		// Widget settings.
		$widget_ops = array(
			'classname' => 'widget_al_flickr',
			'description' => __('Display images from flickr', 'Bellezza')
		);

		// Widget control settings.
		$control_ops = array(
			'id_base' => 'al-flickr-widget'
		);

		// Create the widget.
		$this->WP_Widget('al-flickr-widget', __('Bellezza - Flickr images', 'Bellezza') , $widget_ops, $control_ops);
	}

	// Display the widget on the screen.
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$id = $instance['flickr_id'];
		$nr = ($instance['flickr_nr'] != '') ? $nr = $instance['flickr_nr'] : $nr = 16;
		echo $before_widget;
		if ($title) echo $before_title . $title . $after_title;
		echo "
		<script type=\"text/javascript\">
			jQuery(document).ready(function(){
				jQuery('ul#basicuse').jflickrfeed({
					limit: ".$nr.",
					qstrings: {
						id: '".$id."'
					},
					itemTemplate: '<li><a href=\"http://www.flickr.com/photos/".$id."\"><img src=\"{{image_s}}\" alt=\"{{title}}\" /></a></li>'
				});
			});
		</script>";
		echo '<ul id="basicuse" class="thumbs"><li></li></ul>'.$after_widget;
		
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['flickr_id'] = strip_tags($new_instance['flickr_id']);
		$instance['flickr_nr'] = strip_tags($new_instance['flickr_nr']);
		return $instance;
	}

	function form($instance) {
		$defaults = array(
		'title' => 'Latest From Flickr',
		'flickr_nr' => '16',
		'flickr_id' => '47445714@N03'
		);
		
		$instance = wp_parse_args((array)$instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'Bellezza'); ?></label>
			<input id="<?php echo $this->get_field_id('title'); ?>" type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
		</p>
        
		<p>
			<label for="<?php echo $this->get_field_id('flickr_id'); ?>"><?php _e('Flickr ID:', 'Bellezza'); ?></label> 
			<input id="<?php echo $this->get_field_id('flickr_id'); ?>" type="text" name="<?php echo $this->get_field_name('flickr_id'); ?>" value="<?php echo $instance['flickr_id']; ?>" class="widefat" />
            <small style="line-height:12px;"><a href="http://www.idgettr.com">Find your Flickr user or group id</a></small>
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('flickr_nr'); ?>"><?php _e('Number of photos:', 'Bellezza'); ?></label> 
			<input id="<?php echo $this->get_field_id('flickr_nr'); ?>" type="text" name="<?php echo $this->get_field_name('flickr_nr'); ?>" value="<?php echo $instance['flickr_nr']; ?>" class="widefat" />
		</p>
	<?php
	}
}

register_widget('al_flickr_widget');


/* ***************************************************** 
 * Plugin Name: Last Tweets
 * Description: Displays Latest Tweets.
 * Version: 1.1
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */

add_action( 'widgets_init', 'latest_tweet_widget' );
function latest_tweet_widget() {
	register_widget( 'Latest_Tweets' );
}
class Latest_Tweets extends WP_Widget {

	function Latest_Tweets() {
		$widget_ops = array( 'classname' => 'twitter-widget'  );
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'latest-tweets-widget' );
		$this->WP_Widget( 'latest-tweets-widget','Bellezza - Latest Tweets', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );

		$title = apply_filters('widget_title', $instance['title'] );
		$no_of_tweets = $instance['no_of_tweets'];
		$transName = 'list_tweets';
	    $cacheTime = 20;
		$twitter_username 		= $instance['twitter_username'];
		$consumer_key 			= $instance['consumer_key'];
		$consumer_secret		= $instance['consumer_secret'];
		$access_token 			= $instance['access_token'];
		$access_token_secret 	= $instance['access_token_secret'];
		
	if( !empty($twitter_username) && !empty($consumer_key) && !empty($consumer_secret) && !empty($access_token) && !empty($access_token_secret)  ){
	    if(false === ($twitterData = get_transient($transName) ) ){
		
			$twitterConnection = new TwitterOAuth( $consumer_key , $consumer_secret , $access_token , $access_token_secret	);
			$twitterData = $twitterConnection->get(
					  'statuses/user_timeline',
					  array(
					    'screen_name'     => $twitter_username ,
					    'count'           => $no_of_tweets
						)
					);
			if($twitterConnection->http_code != 200)
			{
				$twitterData = get_transient($transName);
			}
	        // Save our new transient.
	        set_transient($transName, $twitterData, 60 * $cacheTime);
	    }
		/* Before widget (defined by themes). */
		echo $before_widget;
	
	
			echo $before_title; ?>
			<a href="http://twitter.com/<?php echo $twitter_username  ?>" class="twitter-title"><?php echo $title ; ?></a>
		<?php echo $after_title; 

            	if( !empty($twitterData) && is_array($twitterData)  && !isset($twitterData['error'])){
            		$i=0;
					$hyperlinks = true;
					$encode_utf8 = true;
					$twitter_users = true;
					$update = true;
					echo '
			<div id="twitter_div">
                <ul id="twitter_update_list" class="twitter">';
		            foreach($twitterData as $item){
		                    $msg = $item->text;
		                    $permalink = 'http://twitter.com/#!/'. $twitter_username .'/status/'. $item->id_str;
		                    if($encode_utf8) $msg = utf8_encode($msg);
		                    $link = $permalink;
		                     echo '<li class="twitter-item"><i class="icon-twitter"></i>';
		                      if ($hyperlinks) {    $msg = $this->hyperlinks($msg); }
		                      if ($twitter_users)  { $msg = $this->twitter_users($msg); }
		                      echo $msg;
		                    if($update) {
		                      $time = strtotime($item->created_at);
		                      if ( ( abs( time() - $time) ) < 86400 )
		                        $h_time = sprintf( __('%s ago', 'Bellezza'), human_time_diff( $time ) );
		                      else
		                        $h_time = date(__('Y/m/d', 'Bellezza'), $time);
		                      echo sprintf( __('%s', 'twitter-for-wordpress', 'Bellezza'),' <span class="twitter-timestamp"><abbr title="' . date(__('Y/m/d H:i:s', 'Bellezza'), $time) . '">' . $h_time . '</abbr></span>' );
		                     }
		                    echo '</li>
';
		                    $i++;
		                    if ( $i >= $no_of_tweets ) break;
		            }
					echo '</ul> </div>
';
            	}
				else
				{ 
					echo _e('Sorry , Twitter seems down or responds slowly.', 'Bellezza'); 
				}
            ?>
		<?php
		/* After widget (defined by themes). */
		echo $after_widget;
	}
	else{
		echo $before_widget;
		echo $before_title; ?>
			<a href="http://twitter.com/<?php echo $twitter_username  ?>" class="twitter-title"><?php echo $title ; ?></a>
		<?php echo $after_title._e('You need to Setup Twitter API OAuth settings first', 'Bellezza');
		echo $after_widget;
	}
}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['no_of_tweets'] = strip_tags( $new_instance['no_of_tweets'] );
		
		$instance['twitter_username'] = strip_tags( $new_instance['twitter_username'] );
		$instance['consumer_key'] = strip_tags( $new_instance['consumer_key'] );
		$instance['consumer_secret'] = strip_tags( $new_instance['consumer_secret'] );
		$instance['access_token'] = strip_tags( $new_instance['access_token'] );
		$instance['access_token_secret'] = strip_tags( $new_instance['access_token_secret'] );
		//$instance['accounts'] = strip_tags( $new_instance['accounts'] );
		//$instance['replies'] = strip_tags( $new_instance['replies'] );
		return $instance;
	}

	function form( $instance ) {
		$defaults = array( 'title' =>__('@Follow Me' , 'Bellezza') , 'no_of_tweets' => '5', 'twitter_username'=>'', 'consumer_key' => '', 'consumer_secret' => '', 'access_token' => '', 'access_token_secret' => '');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'twitter_username' ); ?>"><?php _e('Username', 'Bellezza')?> : </label>
			<input id="<?php echo $this->get_field_id( 'twitter_username' ); ?>" name="<?php echo $this->get_field_name( 'twitter_username' ); ?>" value="<?php echo $instance['twitter_username']; ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'consumer_key' ); ?>"><?php _e('Consumer Key', 'Bellezza')?> : </label>
			<input id="<?php echo $this->get_field_id( 'consumer_key' ); ?>" name="<?php echo $this->get_field_name( 'consumer_key' ); ?>" value="<?php echo $instance['consumer_key']; ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'consumer_secret' ); ?>"><?php _e('Consumer Secret', 'Bellezza')?> : </label>
			<input id="<?php echo $this->get_field_id( 'consumer_secret' ); ?>" name="<?php echo $this->get_field_name( 'consumer_secret' ); ?>" value="<?php echo $instance['consumer_secret']; ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'access_token' ); ?>"><?php _e('Access Token', 'Bellezza')?> : </label>
			<input id="<?php echo $this->get_field_id( 'access_token' ); ?>" name="<?php echo $this->get_field_name( 'access_token' ); ?>" value="<?php echo $instance['access_token']; ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'access_token_secret' ); ?>"><?php _e('Access Token Secret', 'Bellezza')?> : </label>
			<input id="<?php echo $this->get_field_id( 'access_token_secret' ); ?>" name="<?php echo $this->get_field_name( 'access_token_secret' ); ?>" value="<?php echo $instance['access_token_secret']; ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title', 'Bellezza')?> : </label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'no_of_tweets' ); ?>"><?php _e('Number of Tweets to show', 'Bellezza')?> : </label>
			<input id="<?php echo $this->get_field_id( 'no_of_tweets' ); ?>" name="<?php echo $this->get_field_name( 'no_of_tweets' ); ?>" value="<?php echo $instance['no_of_tweets']; ?>" type="text" size="3" />
		</p>
	<?php
	}
	
		/**
	 * Find links and create the hyperlinks
	 */
	private function hyperlinks($text) {
	    $text = preg_replace('/\b([a-zA-Z]+:\/\/[\w_.\-]+\.[a-zA-Z]{2,6}[\/\w\-~.?=&%#+$*!]*)\b/i',"<a href=\"$1\" class=\"twitter-link\">$1</a>", $text);
	    $text = preg_replace('/\b(?<!:\/\/)(www\.[\w_.\-]+\.[a-zA-Z]{2,6}[\/\w\-~.?=&%#+$*!]*)\b/i',"<a href=\"http://$1\" class=\"twitter-link\">$1</a>", $text);
	    // match name@address
	    $text = preg_replace("/\b([a-zA-Z][a-zA-Z0-9\_\.\-]*[a-zA-Z]*\@[a-zA-Z][a-zA-Z0-9\_\.\-]*[a-zA-Z]{2,6})\b/i","<a href=\"mailto://$1\" class=\"twitter-link\">$1</a>", $text);
	        //mach #trendingtopics. Props to Michael Voigt
	    $text = preg_replace('/([\.|\,|\:|\?|\?|\>|\{|\(]?)#{1}(\w*)([\.|\,|\:|\!|\?|\>|\}|\)]?)\s/i', "$1<a href=\"http://twitter.com/#search?q=$2\" class=\"twitter-link\">#$2</a>$3 ", $text);
	    return $text;
	}
	/**
	 * Find twitter usernames and link to them
	 */
	private function twitter_users($text) {
	       $text = preg_replace('/([\.|\,|\:|\?|\?|\>|\{|\(]?)@{1}(\w*)([\.|\,|\:|\!|\?|\>|\}|\)]?)\s/i', "$1<a href=\"http://twitter.com/$2\" class=\"twitter-user\">@$2</a>$3 ", $text);
	       return $text;
	}
	
}



/* ***************************************************** 
 * Plugin Name: Bellezza Last Works
 * Description: Retrieve and display latest works (Portfolio).
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
class al_works_widget extends WP_Widget {

	// Widget setup.
	function al_works_widget() {

		// Widget settings.
		$widget_ops = array(
			'classname' => 'widget_al_works',
			'description' => __('Display latest works (Portoflio)', 'Bellezza')
		);

		// Create the widget.
		$this->WP_Widget('al-works-widget', __('Bellezza - Latest Works', 'Bellezza') , $widget_ops);
	}

	// Display the widget on the screen.
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['post_title']);
		
		echo $before_widget;
		if ($title) echo $before_title . $title . $after_title;
		$post_count = $instance['post_count'];
		
		$loop = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => $post_count));
		?>
        <ul class="latest-works-widget">
			<?php
            while ( $loop->have_posts() ) : $loop->the_post();?>
            	<li>
                    <div class="work-widget-thumb">
                        <a href="<?php the_permalink()?>">
                            <?php the_post_thumbnail(array(55, 55), array('class'=>'cover') ); ?>
                        </a>
                    </div>      
                    <div class="work-widget-content">
                        <h4><a href="<?php the_permalink()?>" class="color"><?php the_title() ?></a></h4>
                        <?php echo limit_words(get_the_excerpt(), '6')?>
                    </div>
                    <div class="clearnospacing"></div>
               	</li>
               	<!-- <p><?php //echo limit_words($loop->post_content, '12')?></p>-->   
            <?php endwhile;?>
        </ul>
		<div class="clearnospacing"></div>
		<?php echo $after_widget; 
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['post_title'] = strip_tags($new_instance['post_title']);
		$instance['post_count'] = strip_tags($new_instance['post_count']);
		return $instance;
	}

	function form($instance) {
		$defaults = array(
			'post_title' => 'Latest Works',
			'post_count' => '2'
		);
		$instance = wp_parse_args((array)$instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('post_title'); ?>"><?php _e('Title', 'Bellezza'); ?></label>
			<input id="<?php echo $this->get_field_id('post_title'); ?>" type="text" name="<?php echo $this->get_field_name('post_title'); ?>" value="<?php echo $instance['post_title']; ?>" class="widefat" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id('post_count'); ?>"><?php _e('Number of Posts to show', 'Bellezza'); ?></label> 
			<input id="<?php echo $this->get_field_id('post_count'); ?>" type="text" name="<?php echo $this->get_field_name('post_count'); ?>" value="<?php echo $instance['post_count']; ?>" class="widefat" />
		</p>
	<?php
	}
}

register_widget('al_works_widget');



/* ***************************************************** 
 * Plugin Name: Bellezza Blog Posts
 * Description: Retrieve and display latest blog posts.
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
class al_blogposts_widget extends WP_Widget {

	// Widget setup.
	function al_blogposts_widget() {

		// Widget settings.
		$widget_ops = array(
			'classname' => 'latest-blog-posts',
			'description' => __('Display latest blog posts', 'Bellezza')
		);

		// Create the widget.
		$this->WP_Widget('al-blogposts-widget', __('Bellezza Latest Blog Posts', 'Bellezza') , $widget_ops);
	}

	// Display the widget on the screen.
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['post_title']);
		
		echo $before_widget;
		if ($title) echo $before_title . $title . $after_title;
		$post_count = $instance['post_count'];
		$post_category = $instance['post_category'];
		$more_title = $instance['more_title'];
		
		global $post;
		$args = array( 'numberposts' => $post_count);
		if (!empty($post_category))
		$args['category'] = $post_category;
		$myposts = get_posts( $args );
		if ($myposts): ?>
			<div class="imagewidget">
				<ul>
				<?php foreach( $myposts as $post ) :	setup_postdata($post);  ?>
					<li>
						<a href="<?php the_permalink()?>" class="lbp-imageholder">
							<?php the_post_thumbnail('blog-thumb', array('class'=>'framesm') ); ?>
						</a>
						<a href="<?php  the_permalink() ?>"> <?php  the_title() ?><span class="light italic"><br /><?php echo $more_title ?></span></a>
						<div class="clearnospacing"></div>
					</li>
				<?php endforeach; ?>
				</ul>
				
			</div>
		<?php endif; ?>
        <?php echo $after_widget; 
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['post_title'] = strip_tags($new_instance['post_title']);
		$instance['post_count'] = strip_tags($new_instance['post_count']);
		$instance['post_category'] = strip_tags($new_instance['post_category']);
		$instance['more_title'] = strip_tags($new_instance['more_title']);
		return $instance;
	}

	function form($instance) {
		$defaults = array(
			'post_title' => 'Latest from the blog',
			'post_count' => '2',
			'post_category' => '',
			'more_title' => 'View article',
		);
		$instance = wp_parse_args((array)$instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('post_title'); ?>"><?php _e('Title', 'Bellezza'); ?></label>
			<input id="<?php echo $this->get_field_id('post_title'); ?>" type="text" name="<?php echo $this->get_field_name('post_title'); ?>" value="<?php echo $instance['post_title']; ?>" class="widefat" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id('post_count'); ?>"><?php _e('Number of Posts to show', 'Bellezza'); ?></label> 
			<input id="<?php echo $this->get_field_id('post_count'); ?>" type="text" name="<?php echo $this->get_field_name('post_count'); ?>" value="<?php echo $instance['post_count']; ?>" class="widefat" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id('post_category'); ?>"><?php _e('Category (Leave Blank to show from all categories)', 'Bellezza'); ?></label> 
			<input id="<?php echo $this->get_field_id('post_category'); ?>" type="text" name="<?php echo $this->get_field_name('post_category'); ?>" value="<?php echo $instance['post_category']; ?>" class="widefat" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('more_title'); ?>"><?php _e('Read more title', 'Bellezza'); ?></label>
			<input id="<?php echo $this->get_field_id('more_title'); ?>" type="text" name="<?php echo $this->get_field_name('more_title'); ?>" value="<?php echo $instance['more_title']; ?>" class="widefat" />
		</p>
	<?php
	}
}

register_widget('al_blogposts_widget');


/* ***************************************************** 
 * Plugin Name: Information Widget
 * Description: Show information with an icon in a widget.
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
class al_info_widget extends WP_Widget {

	// Widget setup.
	function al_info_widget() {

		// Widget settings.
		$widget_ops = array(
			'classname' => 'widget_al_content',
			'description' => __('Display custom content with icon and button', 'Bellezza')
		);

		// Widget control settings.
		$control_ops = array('id_base' => 'al-content-widget');

		// Create the widget.
		$this->WP_Widget('al-content-widget', __('Bellezza information widget', 'Bellezza') , $widget_ops, $control_ops);
	}

	// Display the widget on the screen.
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$descr = $instance['content_desc'];
		$image = $instance['content_image'];
		$link = $instance['content_link'];
		$linkTitle = $instance['link_title'];
		$active = $instance['is_active'];
		
		echo $before_widget;
		
		//SHOW the posts
		?>
        <div class="showbox nomargin boxpadding infoblock">	
            <h3 class="uppercase"><span class="icon-<?php echo $image ?>"></span><?php echo $title ?></h3>
            <div class="clearextrasmall"></div>
            <p><?php echo $descr ?></p>
            <?php if ($link): ?>
                <a href="<?php echo $link ?>" class="button <?php if ($active):?>highlight<?php else:?>regular<?php endif?> small">
					<?php echo $linkTitle ?>
				</a>
            <?php endif?>
        </div>   
            
            
		<?php
		
		echo $after_widget;
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['link_title'] = strip_tags($new_instance['link_title']);
		$instance['content_desc'] = strip_tags($new_instance['content_desc']);
		$instance['content_image'] = $new_instance['content_image'];
		$instance['content_link'] = $new_instance['content_link'];
		$instance['is_active'] = $new_instance['is_active'];
		
		return $instance;
	}

	function form($instance) {
		$defaults = array(
		'title' => '',
		'content_desc' => '',
		'content_image' => '',
		'content_link' => 'http://',
		'link_title' => 'Read More',
		'is_active' => '',
		);
		
		$instance = wp_parse_args((array)$instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'Bellezza'); ?></label>
			<input id="<?php echo $this->get_field_id('title'); ?>" type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
		</p>
        
       <p>
			<label for="<?php echo $this->get_field_id('content_desc'); ?>"><?php _e('Description:', 'Bellezza'); ?></label> 
			<textarea id="<?php echo $this->get_field_id('content_desc'); ?>" name="<?php echo $this->get_field_name('content_desc'); ?>" class="widefat" style="height:100px"><?php echo $instance['content_desc']; ?></textarea>
  		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id('content_image'); ?>"><?php _e('Icon name:', 'Bellezza'); ?></label> 
			<input id="<?php echo $this->get_field_id('content_image'); ?>" type="text" name="<?php echo $this->get_field_name('content_image'); ?>" value="<?php echo $instance['content_image']; ?>" class="widefat" />
  		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id('content_link'); ?>"><?php _e('Link (optional):', 'Bellezza'); ?></label> 
			<input id="<?php echo $this->get_field_id('content_link'); ?>" type="text" name="<?php echo $this->get_field_name('content_link'); ?>" value="<?php echo $instance['content_link']; ?>" class="widefat" />
  		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id('link_title'); ?>"><?php _e('Link Title:', 'Bellezza'); ?></label> 
			<input id="<?php echo $this->get_field_id('link_title'); ?>" type="text" name="<?php echo $this->get_field_name('link_title'); ?>" value="<?php echo $instance['link_title']; ?>" class="widefat" />
  		</p>
        <p>
			<label for="<?php echo $this->get_field_id('is_active'); ?>"><?php _e('Highlight link:', 'Bellezza'); ?></label> 
			<select id="<?php echo $this->get_field_id('is_active'); ?>"  name="<?php echo $this->get_field_name('is_active'); ?>" class="widefat">
            	<option value="0" <?php if ($instance['is_active']==0): ?>selected="selected"<?php endif?>><?php _e('No', 'Bellezza')?></option>
            	<option value="1" <?php if ($instance['is_active']==1): ?>selected="selected"<?php endif?>><?php _e('Yes', 'Bellezza')?></option>
            </select>
  		</p>
        
	<?php
	}
}

register_widget('al_info_widget');


/* ***************************************************** 
 * Plugin Name: Bellezza Contact Widget
 * Description: Display contact widget on footer.
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
/**
 * Contact Form Widget Class
 */
class al_Contact_Form extends WP_Widget {
	
	function al_Contact_Form() {
		$widget_ops = array('classname' => 'al_contact_form_entries', 'description' => __("Contact widget", 'Bellezza') );
		$this->WP_Widget('al_Contact_Form', __('Bellezza - Contact Form', 'Bellezza'), $widget_ops);
	}

	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', empty($instance['title']) ? __('Contact Us', 'Bellezza') : $instance['title'], $instance);
		$email = apply_filters('widget_title', empty($instance['email']) ? __('', 'Bellezza') : $instance['email'], $instance);
		$success = apply_filters('widget_title', empty($instance['success']) ? __('Thank you, e-mail sent.', 'Bellezza') : $instance['success'], $instance);
		
		echo $before_widget;
		
		if ( $title ) echo $before_title . $title . $after_title;

			echo '<form action="#" method="post" id="contactFormWidget">';
			echo '<div><input type="text" name="wname" id="wname" value="" size="22" title="Name" /></div>';
			echo '<div><input type="text" name="wemail" id="wemail" value="" size="22" title="Email" /></div>';
			echo '<div><textarea name="wmessage" id="wmessage" cols="60" rows="10" title="Message"></textarea></div>';
			echo '<div class="loading"></div>';
			echo '<div><input type="hidden" name="wcontactemail" id="wcontactemail" value="'.$email.'" /></div>';
			echo '<div><input type="hidden" value="'.home_url().'" id="wcontactwebsite" name="wcontactwebsite" /></div>';
			echo '<div><input type="hidden" name="wcontacturl" id="wcontacturl" value="'.get_template_directory_uri().'/library/sendmail.php" /></div>';
			echo '<div><input type="submit" id="wformsend" value="Submit" class="button footerform small" name="wsend"  /></div>';
			echo '<div class="clearnospacing"></div>';
			echo '<div class="widgeterror"></div>';
			echo '<div class="widgetinfo">'.$success.'</div>';
			echo '</form>';
	
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['email'] = strip_tags($new_instance['email']);
		$instance['success'] = strip_tags($new_instance['success']);
		return $instance;
	}

	function form( $instance ) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$email = isset($instance['email']) ? esc_attr($instance['email']) : '';
		$success = isset($instance['success']) ? esc_attr($instance['success']) : '';
	?>
	
		<div>
        	<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:<br />', 'Bellezza'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
		</div>
        <div>
        	<label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email Address:<br />', 'Bellezza'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo $email; ?>" /></label></p>
		</div>
        <div>
        	<label for="<?php echo $this->get_field_id('success'); ?>"><?php _e('Success Message:<br />', 'Bellezza'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('success'); ?>" name="<?php echo $this->get_field_name('success'); ?>" type="text" value="<?php echo $success; ?>" /></label></p>
		</div>
		<div style="clear:both"></div>
<?php
	}
}

register_widget('al_Contact_Form');

/* ***************************************************** 
 * Plugin Name: 3-in-1 Posts
 * Description: Retrieve and display popular/latest posts/latest comments.
 * Version: 1.0
 * Author: Weblusive
 * Author URI: http://www.weblusive.com
 * ************************************************** */
class al_totalposts_widget extends WP_Widget {

	// Widget setup.
	function al_totalposts_widget() {

		// Widget settings.
		$widget_ops = array(
			'classname' => 'widget_al_totalposts',
			'description' => __('Retrieve and display popular/latest posts/latest comments.', 'Bellezza')
		);

		// Create the widget.
		$this->WP_Widget('al-totalposts-widget', __('Bellezza Popular/Latest posts/Last comments', 'Bellezza') , $widget_ops);
	}

	// Display the widget on the screen.
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['post_title']);
		
		echo $before_widget;
		if ($title) echo $before_title . $title . $after_title;
		$post_count = $instance['post_count'];
		$post_category = $instance['post_category'];
		
		global $post;
		$args = array( 'numberposts' => $post_count);
		if (!empty($post_category))
		$args['category'] = $post_category;
		?>
		<ul class="tabs tabs-widget">
			<li><a class="" href="#"><?php _e('Popular', 'Bellezza')?></a></li>
			<li><a class="" href="#"><?php _e('Recent', 'Bellezza')?></a></li>
			<li><a class="" href="#"><?php _e('Comments', 'Bellezza')?></a></li>
		</ul>
        <div class="clearnospacing"></div>
		<div class="panes panes-widget">
			<div class="pane imagewidget">				
				<ul class="widget-post-list">
					<?php $myposts = get_posts( $args ); if ($myposts):
						foreach( $myposts as $post ) :	setup_postdata($post);  ?>                 
							<li>
								<div class="wpl-image">
									<a href="<?php the_permalink()?>">
										<?php if(has_post_thumbnail()):?>
											<?php the_post_thumbnail('blog-thumb', array('class'=>'cover') ); ?>
										<?php else:?>
											<img src = "<?php echo get_template_directory_uri()?>/images/picture_small.jpg" alt="<?php _e('No image available', 'Bellezza')?>" />
										<?php endif?>
									</a>
								</div>      
								<div class="wpl-desc">
									<a href="<?php the_permalink()?>"><?php the_title()?></a>
									<a href="<?php the_permalink()?>" class="button minimal small"><?php _e('Read More', 'Bellezza')?></a>
								</div>        
								<div class="clearnospacing"></div>
							</li>
						<?php endforeach; ?>
					<?php endif; ?>
				</ul>
			</div>
			<div class="pane imagewidget">				
				<ul class="widget-post-list">
					<?php 
						$args ['orderby'] = 'comment_count';
						$myposts = get_posts( $args ); 
						
						if ($myposts):
						foreach( $myposts as $post ) :	setup_postdata($post);  ?>                 
							<li>
								<div class="wpl-image">
									<a href="<?php the_permalink()?>">
										<?php if(has_post_thumbnail()):?>
											<?php the_post_thumbnail('blog-thumb', array('class'=>'cover') ); ?>
										<?php else:?>
											<img src = "<?php echo get_template_directory_uri()?>/images/picture_small.jpg" alt="No Image" />
										<?php endif?>
									</a>
								</div>      
								<div class="wpl-desc">
									<a href="<?php the_permalink()?>"><?php the_title()?></a>
									<a href="<?php the_permalink()?>" class="button minimal small"><?php _e('Read More', 'Bellezza')?></a>
								</div>        
								<div class="clearnospacing"></div>
							</li>
						<?php endforeach; ?>
					<?php endif; ?>
				</ul>
			</div>
			<div class="pane imagewidget">				
				<ul class="widget-post-list">
					<?php 
					global $wpdb;	
					$sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_author_email, comment_date_gmt, comment_approved, comment_type, comment_author_url, SUBSTRING(comment_content,1,70) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT 5";
					$comments = $wpdb->get_results($sql);
					foreach ($comments as $comment) :?>
						<li>
							<div class="wpl-image avatar-listing">
								<a href="<?php echo get_permalink($comment->ID).'#comment-'.$comment->comment_ID?>" title="<?php echo strip_tags($comment->comment_author).' '.__('on ', 'Bellezza').' '.$comment->post_title?>">
									<?php echo get_avatar($comment, '45')?>
								</a>
							</div>
							<div class="wpl-desc">
								<a href="<?php echo get_permalink($comment->ID).'#comment-'.$comment->comment_ID?>" title="<?php echo strip_tags($comment->comment_author).' '.__('on', 'Bellezza').' '.$comment->post_title?>">
									<?php echo strip_tags($comment->comment_author)?>
								</a>
								<a href="<?php the_permalink()?>" class="button minimal small"><?php _e('Read More', 'Bellezza')?></a>
							</div>
							<div class="clearnospacing"></div>
						</li>
					<?php endforeach; wp_reset_query();?>			
				</ul>
			</div>
		</div>
        <?php echo $after_widget; 
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['post_title'] = strip_tags($new_instance['post_title']);
		$instance['post_count'] = strip_tags($new_instance['post_count']);
		$instance['post_category'] = strip_tags($new_instance['post_category']);
		return $instance;
	}

	function form($instance) {
		$defaults = array(
			'post_title' => 'Blog posts',
			'post_count' => '3',
			'post_category' => ''
		);
		$instance = wp_parse_args((array)$instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('post_title'); ?>"><?php _e('Title', 'Bellezza'); ?></label>
			<input id="<?php echo $this->get_field_id('post_title'); ?>" type="text" name="<?php echo $this->get_field_name('post_title'); ?>" value="<?php echo $instance['post_title']; ?>" class="widefat" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id('post_count'); ?>"><?php _e('Number of Posts to show', 'Bellezza'); ?></label> 
			<input id="<?php echo $this->get_field_id('post_count'); ?>" type="text" name="<?php echo $this->get_field_name('post_count'); ?>" value="<?php echo $instance['post_count']; ?>" class="widefat" />
		</p>
        
         <p>
			<label for="<?php echo $this->get_field_id('post_category'); ?>"><?php _e('Category (Leave Blank to show from all categories)', 'Bellezza'); ?></label> 
			<input id="<?php echo $this->get_field_id('post_category'); ?>" type="text" name="<?php echo $this->get_field_name('post_category'); ?>" value="<?php echo $instance['post_category']; ?>" class="widefat" />
		</p>
	<?php
	}
}

register_widget('al_totalposts_widget');
?>