<?php
/* Template Name: With Sidebar (Right) */
?>
<?php get_header(); ?>

<!-- Title -->
<div class="box pt20 mainbox">
	<!-- Promo text -->
	<?php $promo = get_post_meta($post->ID, "_promo", $single = false);?>
	<?php if(!empty($promo[0]) ):?>
	   <div class="calloutcontainer">
			<div class="container_12">
				<div class="grid_12">            
					<?php echo do_shortcode($promo[0]);?>
				</div>
			</div>
		</div>    
	<?php endif?>
	<!-- Title -->
	<div class="headertext">
		<?php the_title() ?>
		<?php $headline = get_post_meta($post->ID, "_headline", $single = false);?>
		<?php if(!empty($headline[0]) ):?>
			<span><?php echo $headline[0] ?></span>
		<?php endif?>
	</div>
	<div class="clearnospacing"></div>

	<div class="container_12">
		<div class="grid_9">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>	
		</div>
		<aside class="grid_3_no_margin sidebarright alignright">
			<?php generated_dynamic_sidebar() ?>
		</aside>
		<div class="clearfix"></div>
	</div>
</div>



<?php get_footer(); ?>