</div>
<footer class="footer">
	<section class="footer_container">
		<?php
			$al_options = isset($_POST['options']) ? $_POST['options'] : get_option('al_general_settings');
			$footer_widget_count = isset($al_options['al_footer_widgets_count']) ? $al_options['al_footer_widgets_count']:4;
			for($i = 1; $i<= $footer_widget_count; $i++){
				echo  '<div class="footer-block">';
					if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Widget ".$i) ) :endif;
				echo '</div>';
			}			
		?>
		<div class="clearnospacing"></div>     
	</section>   	
	<div class="bottombar_container">
		<div class="bottombar">
			<section class="copyright">
				<?php echo do_shortcode ($al_options['al_copyright'])?>
			</section>
			<section class="bottom_nav">
			 
			   <?php 
					wp_nav_menu( 
					array( 
						'theme_location' => 'footer_nav',
						'menu' =>'footer_nav', 
						'container'=>'div',
						'container_class'=>'crumb_navigation',
						'depth' => 1, 
						'menu_class' => 'link-list'
						)  
					); 
				?>
			   
			</section>  
			<div class="clearnospacing"></div>			
		</div>
		<?php if (isset($al_options['al_footer_top_icon']) && $al_options['al_footer_top_icon'] == '1'):?> 
			<p><a href="#top" id="top-link"><?php _e('Top', 'Bellezza')?></a></p> 
		<?php endif?>
	</div> 
</footer>
 
<?php if (isset($al_options['al_custom_js'])) echo $al_options['al_custom_js']; ?>
<?php wp_footer()?>
</body>
</html>