<?php
/** The template for displaying 404 pages (Not Found). **/

get_header(); ?>

<!-- Title -->
<div class="box pt20 main-box">
	<!-- Title -->
	<div class="headertext">
		<?php _e('Error 404: Page Not Found', 'Bellezza') ?>
	</div>

    <div class="container_12">
        <div class="contentbox">
           	<h2>
            	<?php _e('Sorry, the page or file you requested may have been moved or deleted.', 'Bellezza')?>
            </h2>
			
			<p>
				<?php _e('It seems the page you were trying to reach has been moved or does not exist any more. Please use our search form to find what you were looking for. If the problem persists, don\'t hesitate to contact us.', 'Bellezza')?>
			</p>
		
		</div>
		<div class="clear"></div>
    </div>
</div>

<?php get_footer(); ?>