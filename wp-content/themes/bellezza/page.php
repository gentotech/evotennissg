<?php /** The default template for pages. **/ ?>

<?php get_header(); ?>

<?php $al_options = get_option('al_general_settings');?>

<!-- Title -->
<div class="box pt20 mainbox">
	<?php $promo = get_post_meta($post->ID, "_promo", $single = false);?>
	<?php if(!empty($promo[0]) ):?>
	   <div class="calloutcontainer">
			<div class="container_12">
				<div class="grid_12">            
					<?php echo do_shortcode($promo[0]);?>
				</div>
			</div>
		</div>    
	<?php endif?>
	<!-- Title -->
	<div class="headertext">
		<?php the_title() ?>
		<?php $headline = get_post_meta($post->ID, "_headline", $single = false);?>
		<?php if(!empty($headline[0]) ):?>
			<span><?php echo $headline[0] ?></span>
		<?php endif?>
	</div>
	<div class="clearnospacing"></div>

	<!-- Promo text -->
	
	<div class="container_12">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
</div>

    
<?php get_footer(); ?>