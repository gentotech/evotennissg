<?php
/*** The template for displaying Search Results pages. ***/

get_header(); ?>
	
<?php
/*** The template for displaying Search Results pages. ***/

get_header(); ?>
	
	
<!-- Title and promo text -->
<div class="box">
	<!-- Title -->
	<div class="headertext">	
        <?php printf( __( 'Search Results for: %s', 'Bellezza' ), '<em>' . get_search_query() . '</em>' ); ?>            
    </div>

	<div class="container_12">
		<div class="grid_9">  
			<?php if ( have_posts() ) : ?>
				<?php get_template_part( 'loop-default', 'search' );?>
			<?php else : ?>
				<div id="post-0" class="post no-results not-found">
					<h4><?php _e( 'Nothing Found', 'Bellezza' ); ?></h4>
					<div class="entry-content">
						<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'Bellezza' ); ?></p>
					 </div><!-- .entry-content -->
				</div><!-- #post-0 -->
			<?php endif; ?>
		</div>
		<aside class="grid_3 sidebarright alignright">
		   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?> <?php   endif;?>
		</aside> 
		<div class="clearfix"></div>
	</div>
</div>

<?php get_footer(); ?>