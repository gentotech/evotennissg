<?php //header("Content-type: text/css");
require_once( ABSPATH . 'wp-load.php');
$al_options = get_option('al_general_settings');
?>
<style type="text/css">
<?php if ( $al_options['al_custom_background'] != '' || $al_options['al_background_color'] != '' || $al_options['al_background_repeat'] != '') :?>
body{
	<?php if(!(empty($al_options['al_custom_background']))):?>background-image:url('<?php echo $al_options['al_custom_background']?>')!important;<?php endif?>  	<?php if(!(empty($al_options['al_background_color']))):?>background-color:<?php echo $al_options['al_background_color']?> !important;<?php endif?>
	<?php if(!(empty($al_options['al_background_repeat']))):?>background-repeat:<?php echo $al_options['al_background_repeat']?> !important;<?php endif?>
	<?php if(!(empty($al_options['al_fixed_bg'])) && !(empty($al_options['al_custom_background']))):?>
		background: url(<?php echo $al_options['al_custom_background']?>) no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	<?php endif?>
}
<?php endif?>

<?php if ($al_options['al_topbar_bg']):?>
.bar{
	<?php if (!(empty($al_options['al_topbar_bg']))):?> background-image:url('<?php echo $al_options['al_topbar_bg']?>'); <?php endif ?> 
	<?php if (!(empty($al_options['al_topbar_bg_repeat']))):?> background-repeat:<?php echo $al_options['al_topbar_bg_repeat'] ?>; <?php endif ?> 
}
<?php endif?>
<?php if ($al_options['al_header_bg']):?>
header{
	<?php if (!(empty($al_options['al_header_bg']))):?> background-image:url('<?php echo $al_options['al_header_bg']?>'); <?php endif ?> 
	<?php if (!(empty($al_options['al_header_bg_repeat']))):?> background-repeat:<?php echo $al_options['al_header_bg_repeat'] ?>; <?php endif ?> 
}
<?php endif?>
<?php if($al_options['al_footer_bg'] != ''):?>
.footer{
	background-image:url('<?php echo $al_options['al_footer_bg']?>'); 
	background-repeat:<?php echo $al_options['footer_bg_repeat'] ?>;	
}
<?php endif?>
<?php if($al_options['al_bottombar_bg_color'] != ''):?>
.footer_container{background-color:<?php	echo  $al_options['al_bottombar_bg_color'] ?>;}
<?php endif?>
<?php if($al_options['al_topbar_bg_color'] != ''):?>
.headertext{background-color:<?php	echo  $al_options['al_topbar_bg_color'] ?>;}
<?php endif?>
<?php if($al_options['al_topmenu_font_size'] != ''):?>
#menu ul a{font-size: <?php echo $al_options['al_topmenu_font_size']?>}
<?php endif?>
<?php if($al_options['al_dropdownmenu_font_size'] != ''):?>
#menu ul ul a{font-size: <?php echo $al_options['al_dropdownmenu_font_size']?>}
<?php endif?>
<?php if($al_options['al_body_font_size'] != ''):?>
body, p, ul#twitter_update_list li, .crumb_navigation ul a, .copyright, .widget ul li a{font-size: <?php echo $al_options['al_body_font_size']?>}
<?php endif?>
<?php if($al_options['al_menu_font_color'] != ''):?>
#menu ul a{color: <?php echo $al_options['al_menu_font_color']?>}
<?php endif?>
<?php if($al_options['al_menu_active_font_color'] != ''):?>
#menu ul .current > a, #menu ul .current-menu-item > a, #menu ul .current_page_item > a, #menu ul .current_page_parent > a, #menu ul .current-menu-parent > a{
	color: <?php echo $al_options['al_menu_active_font_color']?>}
<?php endif?>
<?php if($al_options['al_submenu_font_color'] != ''):?>
#menu ul ul a{color: <?php echo $al_options['al_submenu_font_color']?>}
<?php endif?>
<?php if($al_options['al_menu_shadow_color'] != ''):?>
#menu ul a, #menu ul ul a{text-shadow:0 0 0 transparent,<?php echo $al_options['al_menu_shadow_color']?> 0px 1px 0px}
<?php endif?>
<?php if ( $al_options['nivo_caption'] == 1):?>
.nivo-caption{display:none !important}
<?php endif?> 
<?php if($al_options['al_main_color']):?>
.color, .masonry-item.masonry-2-column h5 a, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover,
.minimal.button:hover, #testimonials .slide span, .widget ul li a:hover, .widget ul li a:active, .imagewidget ul li a:hover, .imagewidget ul li a:active,
.calloutcontainer .sub, .toggle-trigger.open a:link, .toggle-trigger.open a:visited, #menu ul a:hover, #menu ul .current a, .footer p a:hover, .slidecaption a,
#menu ul .current_page_parent > a, #menu ul .current-menu-parent > a, #menu ul .current_page_parent > a:hover, #menu ul .current-menu-parent > a:hover,
.darkbox a, .infoblock > h3 span, #menu ul .current > a, #menu ul .current-menu-item > a, #menu ul .current_page_item > a, #menu ul .current_page_parent > a, #menu ul .current-menu-parent > a
{color:<?php echo $al_options['al_main_color']?> !important;}
span.highlight, .highlight.button, #eemail_txt_Button, #top-link, .tip, .pricing-tone2, .hover_slideshow a span, .hover_image a span, .hover_link a span, .hover_video a span, .wall ul li a span, 
.fproject ul li a span, .project ul li a span, .wp-pagenavi .current, .filter li.active a, 	#menu ul ul a:hover, #menu ul .current ul a:hover, .search button.btn,
.tabs a:active, .tabs .current, .tabs li a.current, .dash, ol.bjqs-markers li.active-marker a, .stepbox .step, .call_round.button, .circle a, .regular.button:hover,
.round.button, #eemail_txt_Button, .pricing-table .column.featured .table-head, .socialbar img:hover
{background-color:<?php echo $al_options['al_main_color']?>;}

.mininav ul > li:last-child, .highlight.button, .footer h1, .footer h2, .footer h3, .footer h4,  .centerbar h4,
.headertext span, .project >ul li a strong, .fproject ul li a strong, .wall ul li a strong, .wall ul > li, .wall ul li a strong, .regular.button:hover

{border-color:<?php echo $al_options['al_main_color']?>}
.thumbs li a img:hover, .sidebarthumbs li a img:hover{outline:<?php echo $al_options['al_main_color']?> 3px solid; }

.ls-defaultskin .ls-nav-sides,.ls-defaultskin .ls-bottom-slidebuttons a,.ls-defaultskin .ls-nav-prev,.ls-defaultskin .ls-nav-next,
.ls-defaultskin .ls-nav-start, .ls-defaultskin .ls-nav-stop, .nivo-directionNav a {background-color:<?php echo $al_options['al_main_color']?> !important}	
.textpromotionlg{color:<?php echo $al_options['al_main_color']?> !important}
#menu ul ul a:hover {color:#FFF !important}
<?php endif?>
<?php if($al_options['al_custom_css']):?>
	<?php echo $al_options['al_custom_css'] ?>
<?php endif?>
</style>