<?php 
/* Template Name: Portfolio (Wall) */

get_header(); 

get_template_part('portfolio_header');
$tn = $_SESSION['ptname'] = 'portfolio-template-wall.php';
?>


<!--Page Content-->

<?php
	$pageId = get_page_ID_by_page_template($tn);
	$items_per_page = 777;
	$loop = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => 10)); 
	if( get_post_meta($post->ID, "_page_portfolio_num_items_page", $single = true) != '' ) 
	{ 
		$items_per_page = get_post_meta($post->ID, "_page_portfolio_num_items_page", $single = false);
		
	} 
	else 
	{ // else don't paginate
		$items_per_page = 777;
	}
?>

<?php $cats = get_post_meta($post->ID, "_page_portfolio_cat", $single = true); if( $cats == '' ): ?>
	<div class="container_12">
		<div class="portfolio-content">
			<p>	<?php _e('No categories selected. To fix this, please login to your WP Admin area and set
				the categories you want to show by editing this page and setting one or more categories 
				in the multi checkbox field "Portfolio Categories".', 'Bellezza')?>
			</p>
		</div>';
	</div>
<?php else: ?>		
	<div class="wall">
		<?php 
			// If the user hasn't set a number of items per page, then use JavaScript filtering
			if( $items_per_page[0] == 777 ) : endif; 
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			//  query the posts in selected terms
			$portfolio_posts_to_query = get_objects_in_term( explode( ",", $cats ), 'portfolio_category');
		 ?>
		 <?php if (!empty($portfolio_posts_to_query)):
		
			$wp_query = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'menu_order', 'order' => 'ASC', 'post__in' => $portfolio_posts_to_query, 'paged' => $paged, 'showposts' => $items_per_page[0] ) ); 
			$counter = 1;
	   
			if ($wp_query->have_posts()):  ?>
			<?php while ($wp_query->have_posts()) : 							
				$wp_query->the_post();
				$custom = get_post_custom($post->ID);
				$link = '';
				$lightbox = 1;
				$image_id = get_post_thumbnail_id($post->ID); 
				$thumb = wp_get_attachment_image_src($image_id,'portfolio-wall');
				
				// Get the portfolio item categories
				$cats = wp_get_object_terms($post->ID, 'portfolio_category');
				if ($cats):
					$cat_slugs = '';
					foreach( $cats as $cat ) {
						$cat_slugs .= $cat->slug . " ";
					}
				endif;
				?>
				<div class="view <?php if($thumb):?>view-tenth<?php endif?>">
					<?php if( !empty ( $custom['_portfolio_video'][0] ) ) : $link = $custom['_portfolio_video'][0];  // Check if there's a video to be displayed in the lightbox when clicking the thumb ?>
						<a href="<?php echo $custom['_portfolio_video'][0]; ?>" title="<?php the_title(); ?>" data-rel="prettyPhoto">
					<?php elseif( isset($custom['_portfolio_link'][0]) && $custom['_portfolio_link'][0] != '' ) : $link = $custom['_portfolio_link'][0]; $lightbox=0; // User has set a custom destination link for this portfolio item ?>
						<a href="<?php echo $custom['_portfolio_link'][0]; ?>" title="<?php the_title(); ?>">
					<?php elseif(  isset( $custom['_portfolio_no_lightbox'][0] )  &&  $custom['_portfolio_no_lightbox'][0] !='' ) :  $link = get_permalink($post->ID); $lightbox=0; // View the project details ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php else : $full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false); $link = $full_image[0]; ?>
						<a href="<?php  echo $full_image[0]; ?>"title="<?php the_title(); ?>" data-rel="prettyPhoto['pp_gal']">
					<?php endif; ?>
						<?php if(has_post_thumbnail($post->ID)) the_post_thumbnail('portfolio-wall', array('class' => 'cover')); ?>
					</a>	
					<div class="mask portfolio-mask">
						<h2><?php the_title(); ?></h2>
						<?php the_excerpt()?>
						<a href="<?php echo $link ?>" <?php if($lightbox==1):?> data-rel="prettyPhoto"<?php endif?> class="button round" title="<?php the_title(); ?>">
							<?php echo isset($al_options['al_portfolioreadmore']) ? $al_options['al_portfolioreadmore'] : _e ('Go', 'Bellezza') ?>
						</a>
					</div>			
				</div>
			<?php endwhile; ?>
		 <?php endif;?>
		 <?php endif;?>			
	</div>
	<div class="clearnospacing"></div>
	<?php if( !empty ($items_per_page) &&  $items_per_page[0]!= 777) : ?>
		<div class="box">
			<?php 
			 	include(Bellezza_PLUGINS . '/wp-pagenavi.php' );
				wp_pagenavi();
			?>
			<div class="clear"></div>
		</div>
	<?php endif ?> 
<?php endif?>
<?php $pageContent =  getPageContent($pageId); ?>
<?php if (!(empty ($pageContent))):?>
	<div class="box">
		<?php echo $pageContent ?>
		<div class="clearnospacing"></div>
	</div>
<?php endif?>
<?php get_footer(); ?>