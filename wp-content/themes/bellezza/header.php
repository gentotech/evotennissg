<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"  <?php language_attributes( ) ?>> <!--<![endif]-->
<head>

    <meta charset="utf-8">
   	<meta name="description" content="">
	<meta name="author" content="">
  	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=UTF-8" /> 
	
	<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
    
	<link rel="alternate" type="application/rss+xml" title="RSS2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

	<?php  $al_options = get_option('al_general_settings'); ?>
	
   	<?php if(!empty($al_options['al_favicon'])):?>
	<link rel="shortcut icon" href="<?php echo $al_options['al_favicon'] ?>" /> 
 	<?php endif?>
    <!--[if IE ]><link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/ie.css" /><![endif]-->
  	
   	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
   	
   	<!--[if lt IE 9]><script src="<?php echo get_template_directory_uri() ?>/js/html5.js"></script><![endif]-->
   <?php  $background = isset($al_options['al_background']) && $al_options['al_background'] != '' ? $al_options['al_background'] : '';?>	
  	
    <?php if ($background !== ''): ?>
		<style type="text/css">body{background-image:url(<?php echo get_template_directory_uri() ?>/images/dark_backgrounds/<?php echo $background?>.jpg)}</style> 	
    <?php endif ?>
	
	
	<?php include (get_template_directory() . '/css/dynamic-styles.php'); ?>
	
	
	<?php $pageBg = @get_post_meta($post->ID, "_background", $single = false);?>
	<?php if(!empty($pageBg[0]) ):?>
		<style type="text/css">body{background-image:url(<?php echo $pageBg[0]?>)!important}</style>
	<?php endif?>
    <?php 
   		$bodyFont = isset($al_options['al_body_font']) ? $al_options['al_body_font'] : 'off';
		$headingsFont =(isset($al_options['al_headings_font']) && $al_options['al_headings_font'] !== 'off') ? $al_options['al_headings_font'] : 'off';
		$menuFont = (isset($al_options['al_menu_font']) && $al_options['al_menu_font'] !== 'off') ? $al_options['al_menu_font'] : 'off';
	
		$fonts['body, p, a'] = $bodyFont;
		$fonts['h1, h2, h3, h4, .calloutcontainer .text, .headertext, .headertext span'] = $headingsFont;
		
		$fonts['#menu ul a'] = $menuFont;
		
		foreach ($fonts as $value => $key)
		{
			if($key != 'off' && $key != ''){ 
				$api_font = str_replace(" ", '+', $key);
				$font_name = font_name($key);
				
				echo '<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family='.$api_font.'" />';
				echo "<style type=\"text/css\">".$value."{ font-family: '".$key."' !important; }</style>";			
			}
		}
	?>
	<?php wp_head(); ?>
</head>
<body  <?php body_class(); ?>>
<?php if (isset($al_options['al_contact_icon_url']) && $al_options['al_contact_icon_url'] !==''):?>
	<div class="circle"><a href="<?php echo $al_options['al_contact_icon_url']?>"><?php _e ('Contact', 'Bellezza')?> </a></div>
<?php endif?>
<div class="pageshadow">
    <div class="bar">
        <!--Top Nav-->
		<div class="container_12">
			<div class="grid_6 header-topleft-info">				
				<?php if (isset ($al_options['al_header_menu']) && $al_options['al_header_menu'] !=''):?>
					<?php echo do_shortcode ($al_options['al_header_menu']) ?>
				<?php endif?>				
			</div>
			
			<div class="grid_6_no_margin alignright">
			<!--Social Networks-->
				<div class="socialbar">
					<?php if (isset ($al_options['al_header_social']) && $al_options['al_header_social'] !=''):?>
						<?php echo do_shortcode ($al_options['al_header_social']) ?>
					<?php endif?>           
				</div>
			<!--End Social Networks-->        
			</div>          
			
		</div>
	</div>	
	<!--Start Box-->
		
	<header class="navbar">
		<a href="#" id="top"></a>
		<!--Logo-->
		<div class="grid_3_no_margin"> 
			<a href="<?php echo site_url() ?>" class="block">
				<?php if(!empty($al_options['al_logo'])):?>
					<img src="<?php echo $al_options['al_logo'] ?>" alt="<?php echo $al_options['al_logotext']?>" id="logo-image" class="logo" />
				<?php else:?>
					<?php echo isset($al_options['al_logotext']) ? $al_options['al_logotext'] : 'Bellezza' ?>
				<?php endif?>
			</a>
		</div>
		<!--End Logo-->
		<!--Main Nav--> 
		<nav class="grid_9_no_margin">  
			<?php 
				if(function_exists('wp_nav_menu')):
					wp_nav_menu( 
					array( 
						'menu' =>'primary_nav', 
						'container'=>'div', 
						'depth' => 4, 
						'container_id' => 'menu',
						'menu_class' => 'sf-menu'
						)  
					); 
				else:
					?>
					<div id="menu">
						<ul class="sf-menu top-level-menu"><?php wp_list_pages('title_li=&depth=4'); ?></ul> 
					</div>
					<?php
				endif; 
			?>
			<div class="clearnospacing"></div>
		</nav>
		<div class="clearnospacing"></div>
	</header>
	<div class="clearnospacing"></div>	