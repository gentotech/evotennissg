<?php
/**
 * Portfolio Single Posts template.
 */

get_header(); ?>

<?php  $al_options = get_option('al_general_settings'); ?>
<!-- Title -->
<div class="box">
	<!-- Title -->
	<div class="headertext">
		<?php the_title() ?>
		<?php $headline = get_post_meta($post->ID, "_headline", $single = false);?>
		<?php if(!empty($headline[0]) ):?>
			<span><?php echo $headline[0] ?></span>
		<?php endif?>
		<div class="clearnospacing"></div>
	</div>
	<div class="container_12">
    	<div class="grid_12_no_margin">
			<?php if ( have_posts() ) while ( have_posts() ) :   the_post();  ?>
				<?php the_content();  ?>                              
            <?php  endwhile ?>    
			<?php //comments_template( '', true ); ?>
			<!--<div class="clear"></div>-->
        </div>        
        <div class="clearnospacing"></div> 
	</div>
</div>        
<?php get_footer(); ?>