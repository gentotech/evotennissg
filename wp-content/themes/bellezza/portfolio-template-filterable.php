<?php 
/* Template Name: Portfolio (Filterable) */

get_header(); 

$tn = $_SESSION['ptname'] = 'portfolio-template-filterable.php';
if(!(is_page_template ('homepage-template4.php')))
{
	get_template_part('portfolio_header');
}

$renderjs = '';
?>


<!--Page Content-->
<?php if(!(is_page_template ('homepage-template4.php'))): ?><div class="box"><?php endif?>
	<div class="container_12">
		<?php
			$pageId = get_page_ID_by_page_template($tn);
			echo getPageContent($pageId); 
	  
			$loop = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => 10)); 
			if( get_post_meta($post->ID, "_page_portfolio_num_items_page", $single = true) != '' ) 
			{ 
				$items_per_page = get_post_meta($post->ID, "_page_portfolio_num_items_page", $single = false);
			} 
			else 
			{ // else don't paginate
				$items_per_page = 777;
			}
		?>
		<div class="grid_12">
			<ul class="splitter portfolio-main filter">
				<li class="active all-projects"><a href="#" title="<?php _e('All', 'Bellezza')?>"><?php _e('All', 'Bellezza')?></a></li> 
				<?php 
				$cats = get_post_meta($post->ID, "_page_portfolio_cat", $single = true);
				$MyWalker = new PortfolioWalker();
				$args = array( 'taxonomy' => 'portfolio_category', 'hide_empty' => '0', 'include' => $cats, 'title_li'=> '', 'walker' => $MyWalker, 'show_count' => '1');
				$categories = wp_list_categories ($args);
				?>										
			</ul>
		</div>
		<?php if( $cats == '' ): ?>
			<div class="portfolio-content">
				<p>	<?php _e('No categories selected. To fix this, please login to your WP Admin area and set
					the categories you want to show by editing this page and setting one or more categories 
					in the multi checkbox field "Portfolio Categories".', 'Bellezza')?>
				</p>
			</div>';
		<?php else: ?>
			 <div class="grid_12_no_margin">
                <ul id="list" class="image-grid threecol portfolio-content">
					<?php 
						// If the user hasn't set a number of items per page, then use JavaScript filtering
						if( $items_per_page == 777 ) : endif; 
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						//  query the posts in selected terms
						$portfolio_posts_to_query = get_objects_in_term( explode( ",", $cats ), 'portfolio_category');
					 ?>
					 <?php if (!empty($portfolio_posts_to_query)):
					
						$wp_query = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'menu_order', 'order' => 'ASC', 'post__in' => $portfolio_posts_to_query, 'paged' => $paged, 'showposts' => $items_per_page[0] ) ); 
						$counter = 1;
				   
						if ($wp_query->have_posts()):  ?>
						<?php while ($wp_query->have_posts()) : 							
							$wp_query->the_post();
							$custom = get_post_custom($post->ID);
							
							// Get the portfolio item categories
							$cats = wp_get_object_terms($post->ID, 'portfolio_category');
							if ($cats):
								$cat_slugs = '';
								foreach( $cats as $cat ) {
									$cat_slugs .= $cat->slug . " ";
								}
							endif;
							?>
							<?php if (isset($custom['_portfolio_video_m4v']) && ($custom['_portfolio_video_m4v'][0]!='' || $custom['_portfolio_video_ogv'][0]!='')):
								$renderjs.='
									$("#jquery_jplayer_'.get_the_ID().'").jPlayer({
										option: {"fullscreen": true},
										ready: function () {
											$(this).jPlayer("setMedia", {';
												if ($custom['_portfolio_video_m4v'][0]!=''):
													$renderjs.= 'm4v: "'.$custom['_portfolio_video_m4v'][0].'",';
												endif;
												if ($custom['_portfolio_video_ogv'][0]!=''):
													$renderjs.= 'ogv: "'.$custom['_portfolio_video_ogv'][0].'",';
												endif;
												if ($custom['_poster_image'][0]!=''):
													$renderjs.= 'poster: "'.$custom['_poster_image'][0].'"';
												else:
													$renderjs.= 'poster: "'.get_template_directory_uri().'/images/video.jpg"';
												endif;
											$renderjs.='    
											});
										},
										play: function() { // To avoid both jPlayers playing together.
											$(this).jPlayer("pauseOthers");
										},
										repeat: function(event) { // Override the default jPlayer repeat event handler
											if(event.jPlayer.options.loop) {
												$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
												$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
													$(this).jPlayer("play");
												});
											} else {
												$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
												$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
													$("#jquery_jplayer_'.get_the_ID().'").jPlayer("play", 0);
												});
											}
										},
										swfPath: "'.get_template_directory_uri().'/js/jplayer",
										supplied: "ogv, m4v",
										size: {width: "290px",height: "160px",cssClass: "jp-video-filterable"},
										cssSelectorAncestor: "#jp_container_'.get_the_ID().'"
									});';
								?>
								<li class="singlevideo alpha project" data-id="post-<?php echo $counter ?>" data-type="<?php echo $cat_slugs?>">                
									<div id="jp_container_<?php the_ID()?>" class="jp-video  jp-video-filterable">
										<div class="jp-type-single">
											<div id="jquery_jplayer_<?php the_ID()?>" class="jp-jplayer"></div>
											<div class="jp-gui">
												<div class="jp-video-play">
													<a href="javascript:;" class="jp-video-play-icon" tabindex="1"><?php _e('play', 'Bellezza')?></a>
												</div>
												<div class="jp-interface">
													<div class="jp-progress">
														<div class="jp-seek-bar">
															<div class="jp-play-bar"></div>
														</div>
													</div>
													<div class="jp-current-time"></div>
													<div class="jp-duration"></div>
													<div class="jp-controls-holder">
														<ul class="jp-controls">
															<li><a href="javascript:;" class="jp-play" tabindex="1"><?php _e('play', 'Bellezza')?></a></li>
															<li><a href="javascript:;" class="jp-pause" tabindex="1"><?php _e('pause', 'Bellezza')?></a></li>
															<li><a href="javascript:;" class="jp-stop" tabindex="1"><?php _e('stop', 'Bellezza')?></a></li>
															<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute"><?php _e('mute', 'Bellezza')?></a></li>
															<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute"><?php _e('unmute', 'Bellezza')?></a></li>
															<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume"><?php _e('max volume', 'Bellezza')?></a></li>
														</ul>
														<div class="jp-volume-bar">
															<div class="jp-volume-bar-value"></div>
														</div>
														<ul class="jp-toggles">
															<li><a href="javascript:;" class="jp-full-screen" tabindex="1" title="full screen"><?php _e('full screen', 'Bellezza')?></a></li>
															<li><a href="javascript:;" class="jp-restore-screen" tabindex="1" title="restore screen"><?php _e('restore screen', 'Bellezza')?></a></li>
															<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat"><?php _e('repeat', 'Bellezza')?></a></li>
															<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off"><?php _e('repeat off', 'Bellezza')?></a></li>
														</ul>
													</div>
													<div class="jp-title">
														<ul>
															<li><?php the_title()?></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="jp-no-solution">
												<span><?php _e('Update Required', 'Bellezza')?></span>
												<?php _e('To play the media you will need to either update your browser to a recent version or update your Flash plugin.', 'Bellezza')?>
											</div>
										</div>
									</div>
									
								</li>
							<?php elseif (isset($custom['_portfolio_audio_m4a']) && ( $custom['_portfolio_audio_m4a'][0]!='' || $custom['_portfolio_audio_oga'][0]!='')):
								$renderjs.='$("#jquery_jplayer_'.get_the_ID().'").jPlayer({
									ready: function () {
										$(this).jPlayer("setMedia", {';
											if ($custom['_portfolio_audio_m4a'][0]!=''):
												$renderjs.='m4a: "'.$custom['_portfolio_audio_m4a'][0].'",';
											endif;
											if ($custom['_portfolio_audio_oga'][0]!=''):
												$renderjs.='oga: "'.$custom['_portfolio_audio_oga'][0].'",';
											endif;
											if ($custom['_poster_image'][0]!=''):
												$renderjs.='poster: "'.$custom['_poster_image'][0].'"';
											else:
												$renderjs.='poster: "'.get_template_directory_uri().'/images/audio.jpg"';
											endif;
										$renderjs.='});
									},
									play: function() { // To avoid both jPlayers playing together.
										$(this).jPlayer("pauseOthers");
									},
									repeat: function(event) { // Override the default jPlayer repeat event handler
										if(event.jPlayer.options.loop) {
											$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
											$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
												$(this).jPlayer("play");
											});
										} else {
											$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
											$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
												$("#jquery_jplayer_'.get_the_ID().'").jPlayer("play", 0);
											});
										}
									},
									swfPath: "'.get_template_directory_uri().'/js/jplayer",
									supplied: "m4a, oga",
									wmode: "window",
									size: {width: "290px",height: "160px",cssClass: "jp-video-filterable"},
									cssSelectorAncestor: "#jp_container_'.get_the_ID().'"});';
								?>
								<li data-id="post-<?php echo $counter ?>" data-type="<?php echo $cat_slugs?>" class="project singlesong alpha">     
									<div id="jquery_jplayer_<?php the_ID()?>" class="jp-jplayer"></div>           
									<div id="jp_container_<?php the_ID()?>" class="jp-audio  jp-video-filterable">
										<div class="jp-type-single">
											<div class="jp-gui jp-interface">
												<ul class="jp-controls">
													<li><a href="javascript:;" class="jp-play" tabindex="1"><?php _e('play', 'Bellezza')?></a></li>
													<li><a href="javascript:;" class="jp-pause" tabindex="1"><?php _e('pause', 'Bellezza')?></a></li>
													<li><a href="javascript:;" class="jp-stop" tabindex="1"><?php _e('stop', 'Bellezza')?></a></li>
													<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute"><?php _e('mute', 'Bellezza')?></a></li>
													<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute"><?php _e('unmute', 'Bellezza')?></a></li>
													<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume"><?php _e('max volume', 'Bellezza')?></a></li>
												</ul>
												<div class="jp-progress">
													<div class="jp-seek-bar">
														<div class="jp-play-bar"></div>
													</div>
												</div>
												<div class="jp-volume-bar">
													<div class="jp-volume-bar-value"></div>
												</div>
												<div class="jp-time-holder">
													<div class="jp-current-time"></div>
													<div class="jp-duration"></div>
							
													<ul class="jp-toggles">
														<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat"><?php _e('repeat', 'Bellezza')?></a></li>
														<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off"><?php _e('repeat off', 'Bellezza')?></a></li>
													</ul>
												</div>
											</div>
											<div class="jp-title">
												<ul>
													<li><?php the_title()?></li>
												</ul>
											</div>
										   
											<div class="jp-no-solution">
												<span><?php _e('Update Required', 'Bellezza')?></span>
												<?php _e('To play the media you will need to either update your browser to a recent version or update your Flash plugin.', 'Bellezza')?>
											</div>
										</div>
									</div>
									
								</li>
							<?php else:?>
								<li data-id="post-<?php echo $counter ?>" data-type="<?php echo $cat_slugs?>" class="project">
									<?php if( !empty ( $custom['_portfolio_video'][0] ) ) : // Check if there's a video to be displayed in the lightbox when clicking the thumb ?>
										<div class="hover_video">
											<a href="<?php echo $custom['_portfolio_video'][0]; ?>" class="zoom-icon" title="<?php the_title(); ?>" data-rel="prettyPhoto">
												<?php the_post_thumbnail('portfolio-thumb-masonry2', array('class' => 'cover')); ?>
												<span></span>
											</a>
										</div>
									<?php elseif( isset($custom['_portfolio_link'][0]) && $custom['_portfolio_link'][0] != '' ) : // User has set a custom destination link for this portfolio item ?>
										<div class="hover_link">
											<a href="<?php echo $custom['_portfolio_link'][0]; ?>" title="<?php the_title(); ?>">
												<?php the_post_thumbnail('portfolio-thumb-masonry2', array('class' => 'cover')); ?>
												<span></span>
											</a>
										</div>										
									<?php elseif(  isset( $custom['_portfolio_no_lightbox'][0] )  &&  $custom['_portfolio_no_lightbox'][0] !='' ) : // View the project details ?>
										<div class="hover_image">		
											<a href="<?php the_permalink(); ?>">
												<?php the_post_thumbnail('portfolio-thumb-masonry2', array('class' => 'cover')); ?>
												<span></span>
											</a>
										</div>		
									<?php else : 
										$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false); 
										$argsThumb = array(
											'order'          => 'ASC',
											'posts_per_page'  => 99,
											'post_type'      => 'attachment',
											'post_parent'    => $post->ID,
											'post_mime_type' => 'image',
											'post_status'    => null,
											'exclude' => get_post_thumbnail_id()
										);
									?>
										<div class="hover_slideshow">
											<a href="<?php  echo $full_image[0]; ?>" class="zoom-icon" title="<?php the_title(); ?>" data-rel="prettyPhoto[ppgal<?php echo $post->ID?>]">
												<?php the_post_thumbnail('portfolio-thumb-masonry2', array('class' => 'cover')); ?>
												<span></span>
											</a>
											<?php 
												$attachments = get_posts($argsThumb);
												 
												if ($attachments) {
													foreach ($attachments as $attachment) {
														echo '<a href="'.wp_get_attachment_url($attachment->ID, 'full', false, false).'" data-rel="prettyPhoto[ppgal'.$post->ID.']" title="'.get_the_title($post->ID).'"></a>';
													}
												}
											?>
										</div>
									<?php endif; ?>
									<div class="text_box">
										<h4><?php the_title()?></h4>
										<p><?php echo  limit_words(get_the_excerpt(), '8'); ?></p>
									
										<?php if( isset ($custom['_portfolio_readmore'][0]) && $custom['_portfolio_readmore'][0] == '1' ) :?>
											<div class="center">
											<a href="<?php the_permalink()?>" class="button dark small"> <?php _e ('Read More', 'Bellezza') ?></a>      
											</div>		
										<?php endif?>
									</div>
									
								</li>
							<?php endif?>
							
						<?php endwhile; ?>
					 <?php endif;?>
					 <?php endif;?>
				</ul>	
				<div class="clearnospacing"></div>		
			</div>
			
		<?php endif?>
		<div class="clearnospacing"></div> 
	</div>
	
<?php if(!(is_page_template ('homepage-template4.php'))): ?></div><?php endif?>
<div class="clearnospacing"></div>
<script type="text/javascript" src="<?php echo get_template_directory_uri()  ?>/js/jquery.quicksand.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($){
		// Clone applications to get a second collection
		var $data = $(".portfolio-content").clone();
	
		//NOTE: Only filter on the main portfolio page, not on the subcategory pages
		$('.portfolio-main li').click(function(e) {
			$(".filter li").removeClass("active");	
			// Use the last category class as the category to filter by. This means that multiple categories are not supported (yet)
			var filterClass=$(this).attr('class');
			
			if (filterClass == 'all-projects') {
				var $filteredData = $data.find('.project');
			} else {
				//var $filteredData = $data.find('.project[data-type=' + filterClass + ']');
				var $filteredData = $data.find('li[data-type~=' + filterClass + ']');
			}
			$(".portfolio-content").quicksand($filteredData, 
				{
					duration: 800,
					easing: 'swing',
				},
				function() {
					hover_overlay();
					
					$('a[data-rel]').each(function() {
						$(this).attr('rel', jQuery(this).data('rel'));
					});
					$("a[data-rel^='prettyPhoto']").prettyPhoto();
					$(".hover_mail a span, .hover_slideshow a span, .hover_image a span, .hover_link a span, .hover_video a span, .recent ul a, .wall li a span, .project li a span, .wall li a span, .fproject li a span, .project li a strong, .wall li a strong, .fproject li a strong").css("opacity","0");
	
					$(".hover_mail a, .hover_slideshow > a, .hover_image > a, .hover_link > a, .hover_video > a, .wall li a, .project li a, .fproject li a").hover(function () { 
							$(this).find('span, strong').stop().animate({opacity: 1.0}, "slow");		
						},
						// ON MOUSE OUT
						function () {
							$(this).find('span, strong').stop().animate({opacity: 0}, "fast");
						}
					);
					<?php echo $renderjs;?>
				}
			);		
			$(this).addClass("active"); 			
			return false;
		});
		
		<?php echo $renderjs;?>
	});
	
	
</script>

<?php get_footer(); ?>