<?php 
/* Template Name: Contact Form */ 
?>

<?php get_header(); $al_options = get_option('al_general_settings');  ?>

<!-- Title and promo text -->
<div class="box">
	<!-- Promo text -->
	<?php $promo = get_post_meta($post->ID, "_promo", $single = false);?>
	<?php if(!empty($promo[0]) ):?>
	   <div class="calloutcontainer">
			<div class="container_12">
				<div class="grid_12">            
					<?php echo do_shortcode($promo[0]);?>
				</div>
			</div>
		</div>    
	<?php endif?>
	<!-- Title -->
	<div class="headertext">
		<?php the_title() ?>
		<?php $headline = get_post_meta($post->ID, "_headline", $single = false);?>
		<?php if(!empty($headline[0]) ):?>
			<span><?php echo $headline[0] ?></span>
		<?php endif?>
	</div>
	<div class="clearnospacing"></div>

	<div class="container_12">
		<div class="grid_12">	 
			<?php if (!empty($al_options['al_contact_address'])):?>
				<script type="text/javascript">   
					jQuery(function(){
						jQuery('#map_canvas').gmap3(
							{action: 'addMarker',address: "<?php echo htmlspecialchars($al_options['al_contact_address'])?>",map:{center: true,zoom: 14},},
							{action: 'setOptions', args:[{scrollwheel:true}]}
						); 
					});
				</script> 				
				<div class="headershadow">
					<div id="map_canvas" class="gmap3"></div>
				</div>
			<?php endif?> 	
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                    <div class="clearsmall"></div>
					
					<?php if(isset($hasError) || isset($captchaError)): ?>
						<p class="error"><?php _e('There was an error submitting the form.', 'Bellezza')?><p>
					<?php endif ?>
					<p><?php _e('Your email address will not be shared.', 'Bellezza')?></p>
					<form id="contactus" method="post" class="contactsubmit">
						<fieldset>
							
							<div id="registerErrors"></div>
							<div>
								<input type="text" name="contactName" id="contactName" title="<?php _e( 'Name', 'Bellezza' ); ?>" value="<?php if(isset($_POST['contactName'])) echo $_POST['contactName'];?>" class="requiredField txt" />
								<?php if(isset($nameError) && $nameError != ''): ?><span class="error"><?php echo $nameError;?></span><?php endif;?>
							</div>
							<div>
								<input type="text" name="contactNumber" id="contactNumber" title="<?php _e( 'Contact Number', 'Bellezza' ); ?>" value="<?php if(isset($_POST['contactNumber'])) echo $_POST['contactNumber'];?>" class="requiredField txt" />
								<?php if(isset($numberError) && $numberError != ''): ?><span class="error"><?php echo $nameError;?></span><?php endif;?>
							</div>
							<div>    
								<input type="text" name="email" id="email" title="<?php _e('E-mail', 'Bellezza'); ?>" value="<?php if(isset($_POST['email'])) echo $_POST['email'];?>" class="requiredField txt" />
								<?php if(isset($emailError) && $emailError != ''): ?><span class="error"><?php echo $emailError;?></span><?php endif;?>
							</div>
							<div>
								<input type="text" name="subject" title="<?php _e( 'Subject', 'Bellezza' ); ?>" value="<?php if(isset($_POST['subject'])) echo $_POST['subject'];?>" class="txt" id="subject" />
								<?php if(isset($subject) && !$subject = ''): ?><span class="error"><?php echo $subjectError;?></span><?php endif;?>
							</div>
							<div>
								<textarea name="message" title="<?php _e( 'Message', 'Bellezza' ); ?>" rows="8" tabindex="3" id="message" class="txt requiredField"><?php echo isset($_POST['message']) && $_POST['message']!='' ?  stripslashes($_POST['message'])  : ''?></textarea>
								<?php if(isset($messageError) && $messageError != '') { ?>
									<span class="error"><?php echo $messageError;?></span> 
								<?php } ?>
							</div>                       
							<div>
								<?php 
									
									$options = array(
										$al_options['al_contact_error_message'], 
										$al_options['al_contact_success_message'],
										$al_options['al_subject'],
										$al_options['al_email_address']
									);
								?>
								<input type="hidden" name = "options" value="<?php echo implode('|', $options) ?>" />
								<input type="hidden" name="siteurl" value="<?php echo get_option('blogname')?>" />
								<input type="submit" id="send" class="button highlight small" name="sendmail" value="<?php _e( 'Submit', 'Bellezza' ); ?>" />
								<div class="clearnospacing"></div>
							</div>
						</fieldset>
					</form>            	
				<?php endwhile; ?>
            <?php endif; ?>   
        </div>
		<div class="clear"></div>
    </div>
</div>

<script type="text/javascript">

jQuery(document).ready(function(){
  jQuery("#contactus").validate({
	submitHandler: function() {
	
		var postvalues =  jQuery(".contactsubmit").serialize();
		jQuery.ajax
		 ({
		   type: "POST",
		   url: "<?php echo get_template_directory_uri()  ?>/contact-form.php",
		   data: postvalues,
		   success: function(response)
		   {
		 	 jQuery("#registerErrors").addClass('success-message').html(response).show('normal');
		     jQuery('.contactsubmit :input.not("#send")').val("");
		 	 jQuery('.contactsubmit #message').val("");
		   }
		 });
		return false;
		
    },
	focusInvalid: true,
	focusCleanup: false,
	errorLabelContainer: jQuery("#registerErrors"),
  	rules: 
	{
		contactName: {required: true},
        	contactNumber: {required: true},
		email: {required: true, minlength: 6,maxlength: 50, email:true},
		message: {required: true}
	},
	
	messages: 
	{
		contactName: {required: "<?php _e( 'Name is required', 'Bellezza' ); ?>"},
		contactNumber: {required: "<?php _e( 'Contact Number is required', 'Bellezza' ); ?>"},
		email: {required: "<?php _e( 'E-mail is required', 'Bellezza' ); ?>", email: "<?php _e( 'Please provide a valid e-mail', 'Bellezza' ); ?>"},
		message: {required: "<?php _e( 'Message is required', 'Bellezza' ); ?>"}
		
	},
	
	errorPlacement: function(error, element) 
	{
		var er = element.attr("name");
		error.insertAfter(element);
	},
	invalidHandler: function()
	{
		jQuery("body").animate({ scrollTop: 0 }, "slow");
	}
});
});
</script>
<?php get_footer(); ?>