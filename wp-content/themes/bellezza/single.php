<?php
/**
 * The Template for displaying all single posts.
 */

get_header(); ?>
<?php $al_options = get_option('al_general_settings'); ?>	

<div class="box">
	<!-- Title -->
	<div class="headertext">
		<?php the_title() ?>
		<?php $headline = get_post_meta($post->ID, "_headline", $single = false);?>
		<?php if(!empty($headline[0]) ):?>
			<span><?php echo $headline[0] ?></span>
		<?php endif?>
	</div>
	<div class="clearsmall"></div>
	<div class="container_12">
		<?php if($al_options['al_fullwidth_single'] == '1'): ?>
			<div class="grid_9">
		<?php else: ?>
			<div class="grid_12">
		<?php endif ?>
			<?php 
				$postcounter = 1; 
				$attachedImages = array();
				
				while ( have_posts() ) : the_post();
				
				$argsThumb = array(
					'order'          => 'ASC',
					'post_type'      => 'attachment',
					'post_parent'    => $post->ID,
					'post_mime_type' => 'image',
					'post_status'    => null,
					'exclude' => get_post_thumbnail_id()
				);
				$video =  get_post_custom_values("_post_video");
				$type = get_post_custom_values("_post_listing_type");
				if ($type) $type = $type[0];
				$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
			?>
				
				<!--Blog Post-->
				<article <?php post_class('blogpost'); ?> id="post-<?php the_ID();?>">
					<?php if ($type == 3): ?>
						 <div class="hover_video">
							<a href="http://vimeo.com/<?php echo $video[0]?>?width=800&amp;height=450" data-rel="prettyPhoto[blogvideo<?php the_ID();?>]" title="<?php the_title()?>">
								<?php the_post_thumbnail('blog-list2', array('class'=>'')); ?>
							</a>
						</div>
						
					<?php elseif($type==2):?>
						<div class="hover_slideshow">
							<a href="<?php echo $full_image[0]; ?>" title="<?php the_title()?>" data-rel="prettyPhoto[blog<?php the_ID();?>]">
								<?php the_post_thumbnail('blog-list2', array('class'=>'')); ?>
							</a>
							<?php 
								$attachments = get_posts($argsThumb);
								
								if ($attachments) {
									foreach ($attachments as $attachment) {
										//echo apply_filters('the_title', $attachment->post_title);
										echo '<a href="'.wp_get_attachment_url($attachment->ID, 'thumbnail', false, false).'" data-rel="prettyPhoto[blog'.$post->ID.']" title="'.get_the_title($post->ID).'"></a>';
										
									}
								}
							?>
						</div>
					<?php else: ?>
						<div class="hover_link">
							<a href="<?php the_permalink() ?>" title="<?php the_title()?>">
								<?php the_post_thumbnail('blog-list2', array('class'=>'')); ?>
							</a>
						</div>
					<?php endif ?>
					<div class="blog_box">
						<div class="grid_2">
							<!-- Show Date -->
							<?php if($al_options['al_blog_show_date']): ?>
								<h4><?php the_time('M jS, Y'); ?></h4>
							<?php endif ?>
							<div class="dividerslim"></div>
							
							<?php if($al_options['al_blog_show_author']): ?>
								<h6 class="post-author">
									
									<?php printf( __( 'Author: %1$s', 'Bellezza' ),  '<span class="color">'.get_the_author().'</span>') ?>
									
								</h6>
							<?php endif?>
							
							<!-- Show Comments amount -->
							<?php if( 'open' == $post->comment_status && $al_options['al_blog_show_comments']) : ?>
								<h6 class="single-comments">
									<?php _e('Comments: ', 'Bellezza') ?>
									<?php comments_popup_link('0', '1', '%'); ?>
								</h6>
							<?php endif; ?>
							
							 <!-- Show Categories -->
							<?php if($al_options['al_blog_show_cats']): ?>
								<h6>
									<?php if ( count( get_the_category() ) ) : ?>
										<?php $category = get_the_category();  if($category[0]):?>
											<?php _e('Category: ', 'Bellezza') ?><a href="<?php echo get_category_link($category[0]->term_id )?>" class="color"><?php echo $category[0]->cat_name?></a>
										<?php endif?>
									<?php endif; ?>
								</h6>
							<?php endif?>
							
							<div class="dividerslim"></div>
						</div>
						<div class="grid_6">
							<h4>
								<a href="<?php the_permalink(); ?>" class="post-title"><?php the_title()?></a>
							</h4>
							<?php the_content() ?>
						</div>
						<div class="clearsmall"></div>
						<div class="dividerslim"></div>
						
						
						<?php $tags_list = get_the_tag_list( '', '' );
							if ( $tags_list ) : ?>
							<div class="tagcloud single-cloud">Tags: <?php printf( __( '%s', 'Bellezza' ), $tags_list ); ?></div>
						<?php endif; ?>   
						
						<div class="clearsmall"></div>			
					</div>
					<!--End Blog Post-->
				</article>    
				<div class="clear"></div>
				
			<?php $postcounter++; endwhile; // End the loop. Whew. ?>
			<?php comments_template( '', true ); ?>
			<?php $test = false; if ($test) {comment_form(); wp_link_pages( $args );} ?>
		</div>
		<?php if($al_options['al_fullwidth_single'] == '1'): ?>
			<aside class="grid_3_no_margin sidebarright alignright">
			   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?> <?php   endif;?>
			</aside> 
		<?php endif?>
		<div class="clear"></div>
	</div>        
</div>
<?php get_footer(); ?>