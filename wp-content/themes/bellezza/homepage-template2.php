<?php
/* Template Name: Home Page (With Sidebar) */
get_header();

$al_options = get_option('al_general_settings'); 

$slider = isset($al_options['al_active_slider']) && $al_options['al_active_slider'] !='' ? $al_options['al_active_slider'] : '';
//$slider = isset($_GET['slider_type']) ? $_GET['slider_type'] : 'nivo';
if ($slider)
{
	get_template_part('sliders/'.$slider.'/slider');
	wp_reset_query();
}
?>
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Homepage Top Sidebar") ) : ?> <?php   endif;?>

<div class="box">
	<?php $promo = get_post_meta($post->ID, "_promo", $single = false); ?>
	<?php if(!empty($promo[0]) ):?>
			<div class="calloutcontainer">
				<?php echo do_shortcode($promo[0]);?>
				<div class="clearnospacing"></div>
			</div>  
			<div class="dividernospacing"></div>
			<div class="clear"></div>
	<?php endif?>
	<!-- Include homepage widgets -->
	<?php if ($al_options['al_homepage_widgets']): ?>	
	
		<div class="container_12">
			<div class="grid_12 nomargin homepage-widget-container">
				<?php
				for($i = 1; $i<= 4; $i++){
					if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Homepage Widget ".$i) ) :endif;
				}	
				?>       
			</div>    
			<div class="clear"></div>
		</div>		
			
	<?php endif;?>
    
	<!--Page Content-->
	
	<div class="container_12">
		<div class="grid_9">	
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?> 
		 </div>
		<div class="grid_3_no_margin sidebarright alignright">
			<?php generated_dynamic_sidebar() ?>
		</div>
		<div class="clearfix"></div>
	</div>
	
	<!--End Page Content-->
</div>
<?php get_footer(); ?>		