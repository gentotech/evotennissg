<?php /** Functions file for Bellezza theme. **/

/********************* DEFINE MAIN PATHS ********************/

define('Bellezza_PLUGINS', get_template_directory()  . '/plugins' ); // Shortcut to the /plugins/ directory

$adminPath 	= get_template_directory()  . '/library/admin/';
$funcPath 	= get_template_directory()  . '/library/functions/';
$incPath 	= get_template_directory()  . '/library/includes/';

global $al_options;
$al_options = isset($_POST['options']) ? $_POST['options'] : get_option('al_general_settings');
/************************************************************/


/*********** LOAD ALL REQUIRED SCRIPTS AND STYLES ***********/
function loadScripts()
{
	if( !is_admin())
	{
	  	wp_register_style('nivo-styles',  get_template_directory_uri().'/sliders/nivo/nivo-slider.css');
		wp_register_style('nivo-project-styles',  get_template_directory_uri().'/sliders/nivo/nivo-slider-project.css');
		wp_register_style('accordion-styles',  get_template_directory_uri().'/sliders/accordion/styles.css');
		wp_register_style('layer-styles',  get_template_directory_uri().'/sliders/layer/css/layerslider.css');
		wp_register_style('parallax',  get_template_directory_uri().'/sliders/layer/css/parallax.css');
		
		wp_enqueue_style('jplayer-styles',  get_template_directory_uri().'/js/jplayer/skin/blue.monday/jplayer.blue.monday.css',false,'3.0.1','all');
		wp_enqueue_style('pretty-photo-styles',  get_template_directory_uri().'/css/prettyPhoto.css',false,'3.0.1','all');
		
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-UI');
		
		wp_enqueue_script('prettyphoto',  get_template_directory_uri(). '/js/jquery.prettyPhoto.js', array('jquery'), '3.2', true);
		wp_enqueue_script('nivo-slider', get_template_directory_uri() .'/sliders/nivo/nivo-slider.js', array('jquery'), '3.2', true);	
		wp_enqueue_script('scroll',  get_template_directory_uri(). '/js/scroll.js', array('jquery'), '3.2', true);
	  	wp_enqueue_script('top-menu',  get_template_directory_uri(). '/js/menu.js', array('jquery'), '3.2', true);
		wp_enqueue_script('basic-slider',  get_template_directory_uri(). '/js/basic-jquery-slider.min.js', array('jquery'), '3.2', true);
		wp_enqueue_script('masonry',  get_template_directory_uri(). '/js/jquery.masonry.min.js', array('jquery'), '3.2', true);
		wp_enqueue_script('jquery-tools',  get_template_directory_uri(). '/js/jquery.tools.min.js', array('jquery'), '3.2', true);
		wp_enqueue_script('jplayer-audio',  get_template_directory_uri().'/js/jplayer/jquery.jplayer.min.js', array('jquery'), '3.2', true);
		wp_enqueue_script('jplayer-audio-playlist',  get_template_directory_uri().'/js/jplayer/jplayer.playlist.min.js', array('jquery'), '3.2', true);
		wp_enqueue_script('carousel',  get_template_directory_uri(). '/js/jquery.carouFredSel-5.6.2-packed.js', array('jquery'), '3.2', true);
		wp_enqueue_script('my-custom-scripts', get_template_directory_uri(). '/js/custom.js', array('jquery'), '3.2', true);
		
		$al_options = get_option('al_general_settings'); 
		$slider = $al_options['al_active_slider'] !='' ? $al_options['al_active_slider'] : '';
		//$slider = isset($_GET['slider_type']) ? $_GET['slider_type'] : 'nivo';
		
		if($slider == '3d')
		{
			wp_enqueue_script('swfobject', get_template_directory_uri() .'/sliders/3d/swfobject/swfobject.js');		
		}
		
		elseif($slider == 'accordion')
		{
			wp_enqueue_script('acc2-kwicks', get_template_directory_uri() .'/sliders/accordion/jquery.kwicks-1.5.1.pack.js', array('jquery'), '3.2', true);	
			wp_enqueue_script('acc2', get_template_directory_uri().'/sliders/accordion/accordion.js', array('jquery'), '3.2', true);	
			wp_enqueue_style('accordion-styles');
		}
		
		elseif($slider == 'nivo')
		{
			wp_enqueue_style('nivo-styles');
		}
		
		elseif($slider == 'layer')
		{
			wp_enqueue_style('layer-styles');
			wp_enqueue_style('parallax');
			
			wp_enqueue_script('layer-slider', get_template_directory_uri() .'/sliders/layer/js/layerslider.kreaturamedia.jquery-min.js', array('jquery'), '3.2', true);	
		}
		
		// Check if Flickr widget is activated.
		
		if( is_active_widget( '', '', 'al-flickr-widget' ) ) { // check if search widget is used
			wp_enqueue_script('flickr-script', get_template_directory_uri() .'/js/jflickrfeed.min.js', array('jquery'), '3.2', true);		
		}

		if (is_page_template('contact-template.php')){
			$al_options = get_option('al_general_settings'); 
			if (!empty($al_options['al_contact_address']))
			{
				wp_enqueue_script('Google-map-api',  'http://maps.google.com/maps/api/js?sensor=false');
				wp_enqueue_script('Google-map',  get_template_directory_uri().'/js/gmap3.min.js');
			}
			wp_enqueue_script('Validate',  get_template_directory_uri().'/js/validate.js',array('jquery'));
		}
	}
}
add_action( 'wp_enqueue_scripts', 'loadScripts' ); //Load All Scripts

function bellezza_fonts() {
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_style( 'pt-sans', "$protocol://fonts.googleapis.com/css?family=PT+Sans" );
	wp_enqueue_style( 'yanone', "$protocol://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" );
	wp_enqueue_style( 'open-sans', "$protocol://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" );
}
add_action( 'wp_enqueue_scripts', 'bellezza_fonts' );


/************************************************************/


/********************* DEFINE MAIN PATHS ********************/

require_once ($incPath . 'the_breadcrumb.php');
require_once ($incPath . 'OAuth.php');
require_once ($incPath . 'twitteroauth.php');
require_once ($funcPath . 'sidebar-generator.php');
require_once ($funcPath . 'options.php');
require_once ($funcPath . 'post-types.php');
require_once ($funcPath . 'widgets.php');
require_once ($funcPath . 'shortcodes.php');
require_once ($incPath . 'portfolio_walker.php');


require_once ($adminPath . 'custom-fields.php');
require_once ($adminPath . 'scripts.php');
require_once ($adminPath . 'admin-panel/admin-panel.php');

// Redirect To Theme Options Page on Activation
if (is_admin() && isset($_GET['activated'])){
	wp_redirect(admin_url('admin.php?page=adminpanel'));
}

/************** ADD SUPPORT FOR LOCALIZATION ***************/

load_theme_textdomain( 'Bellezza', get_template_directory()  . '/languages' );

	$locale = get_locale();

	$locale_file = get_template_directory()  . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

/************************************************************/


/**************** ADD SUPPORT FOR POST THUMBS ***************/

add_theme_support( 'post-thumbnails');

// Define various thumbnail sizes

add_image_size('portfolio-thumb-sidebar', 280, 180, true); 
add_image_size('portfolio-thumb-masonry2', 290, 160, true); 
add_image_size('portfolio-wall', 260, 200, true); 
add_image_size('portfolio-listing', 210, 168, true); 
add_image_size('portfolio-thumb-filterable2', 310, 240, true); 
add_image_size('blog-thumb', 50, 40, true); 
add_image_size('blog-list', 430, 200, true);
add_image_size('blog-list2', 660, 240, true);

/************************************************************/



/************* ADD SUPPORT FOR WORDPRESS 3 MENUS ************/

add_theme_support( 'nav-menus' );

add_action( 'init', 'my_custom_menus' );
function my_custom_menus() {
    register_nav_menus(
        array(
            'primary_nav' => __( 'Primary Navigation', 'Bellezza'),
    		'footer_nav' => __( 'Footer Navigation', 'Bellezza')
        )
    );
}
/************************************************************/


/************* COMMENTS HOOK *************/

function Bellezza_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
        <div  id="comment-<?php comment_ID(); ?>" class="comment-content contentboxblog">
            <?php echo get_avatar($comment,$size='35'); ?>
            
            <?php printf(__('<cite class="fn">%s</cite>', 'Bellezza'), get_comment_author_link()) ?>
           
            <?php if ($comment->comment_approved == '0') : ?>
            <em><?php _e('Your comment is awaiting moderation.', 'Bellezza') ?></em>
            <br />
            <?php endif; ?>
         
            <div class="comment-meta commentmetadata">
                <a href="<?php echo htmlspecialchars(get_comment_link( $comment->comment_ID )) ?>"></a>
            </div>
			<a class="datestamp"><?php printf(__('%1$s at %2$s', 'Bellezza'), get_comment_date(),get_comment_time()) ?></a>
            <?php edit_comment_link(__('(Edit)', 'Bellezza'),'  ','') ?>
            
         
            <?php comment_text() ?>
            <?php if($args['max_depth']!=$depth) { ?>
            <div class="reply">
            	<?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>            
            <?php } ?>
            </div>
			<div class="clearnospacing"></div>
         </div>	
	<?php	
}

/*****************************************/


/********** SIDEBAR GENERATION ***********/

$al_options = get_option('al_general_settings'); 
$footer_widget_count = isset($al_options['al_footer_widgets_count']) ? $al_options['al_footer_widgets_count']:4;

for($i = 1; $i<= $footer_widget_count; $i++)
{
  if ( function_exists('register_sidebar') )
	register_sidebar(array(
	  	'name' => 'Footer Widget '.$i,
		'id'   => 'footer-sidebar-'.$i,
		'before_widget' => '<div class="footer-block-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4><div class="clearsmall"></div>',
	));
}

if ( function_exists('register_sidebar') )
{	
	register_sidebar(array(
		'name' => 'Blog Sidebar',
		'id'   => 'global-sidebar-1',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
	
	register_sidebar(array(
		'name' => 'Newsletter Sidebar',
		'id'   => 'page-sidebar-1',
        'before_widget' => '<div class="%2$s newsletterbar">',
        'after_widget' => '</div><div class="clear"></div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
}



if (isset($al_options['al_homepage_widgets']) && $al_options['al_homepage_widgets'] == 1)
{
	for($i = 1; $i<= 4; $i++)
	{
	  if ( function_exists('register_sidebar') )
		register_sidebar(array(
			'name' => 'Homepage Widget '.$i,
			'id'   => 'homepage-sidebar-'.$i,
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h4>',
			'after_title' => '</h4>',
		));
	}
}
/*******************************************/


/********** GET PAGES BY PARAMS ************/

/*-- Get root parent of a page --*/
function get_root_page($page_id) 
{
	global $wpdb;
	
	$parent = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE post_type='page' AND ID = '$page_id'");
	
	if ($parent == 0) 
		return $page_id;
	else 
		return get_root_page($parent);
}


/*-- Get page name by ID --*/
function get_page_name_by_ID($page_id)
{
	global $wpdb;
	$page_name = $wpdb->get_var("SELECT post_title FROM $wpdb->posts WHERE ID = '$page_id'");
	return $page_name;
}


/*-- Get page ID by Page Template --*/
function get_page_ID_by_page_template($template_name)
{
	global $wpdb;
	$page_ID = $wpdb->get_var("SELECT post_id FROM $wpdb->postmeta WHERE meta_value = '$template_name' AND meta_key = '_wp_page_template'");
	return $page_ID;
}

/*-- Get page content (Used for pages with custom post types) --*/
if(!function_exists('getPageContent'))
{
	function getPageContent($pageId)
	{
		if(!is_numeric($pageId))
		{
			return;
		}
		global $wpdb;
		$sql_query = 'SELECT DISTINCT * FROM ' . $wpdb->posts .
		' WHERE ' . $wpdb->posts . '.ID=' . $pageId;
		$posts = $wpdb->get_results($sql_query);
		if(!empty($posts))
		{
			foreach($posts as $post)
			{
				return nl2br(do_shortcode($post->post_content));
			}
		}
	}
}


/* -- Get page ID by Custom Field Value -- */
function get_page_ID_by_custom_field_value($custom_field, $value)
{
	global $wpdb;
	$page_ID = $wpdb->get_var("
	    SELECT wposts.ID
    	FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
	    WHERE wposts.ID = wpostmeta.post_id 
    	AND wpostmeta.meta_key = '$custom_field' 
	    AND (wpostmeta.meta_value like '$value,%' OR wpostmeta.meta_value like '%,$value,%' OR wpostmeta.meta_value like '%,$value' OR wpostmeta.meta_value = '$value')		
    	AND wposts.post_status = 'publish' 
	    AND wposts.post_type = 'page'
		LIMIT 0, 1");

	return $page_ID;
}
/*******************************************/


/********* STRING MANIPULATIONS ************/

function al_trim($text, $length, $end = '[...]') {
	$text = preg_replace('`\[[^\]]*\]`', '', $text);
	$text = strip_tags($text);
	$text = substr($text, 0, $length);
	$text = substr($text, 0, last_pos($text, " "));
	$text = $text . $end;
	return $text;
}

function last_pos($string, $needle){
   $len=strlen($string);
   for ($i=$len-1; $i>-1;$i--){
       if (substr($string, $i, 1)==$needle) return ($i);
   }
   return FALSE;
}

function limit_words($string, $word_limit) {
 
	// creates an array of words from $string (this will be our excerpt)
	// explode divides the excerpt up by using a space character
 
	$words = explode(' ', $string);
 
	// this next bit chops the $words array and sticks it back together
	// starting at the first word '0' and ending at the $word_limit
	// the $word_limit which is passed in the function will be the number
	// of words we want to use
	// implode glues the chopped up array back together using a space character
 
	return implode(' ', array_slice($words, 0, $word_limit)).'...';
}



add_filter('the_content', 'shortcode_empty_paragraph_fix');

function shortcode_empty_paragraph_fix($content)
{   
	$array = array (
		'<p>[' => '[', 
		']</p>' => ']', 
		']<br />' => ']'
	);

	$content = strtr($content, $array);
	$content = str_replace( array( '<p></p>' ), '', $content );
    $content = str_replace( array( '<p>  </p>' ), '', $content );
	
	return $content;
}

/*******************************************/


/******* POSTS RELATED BY TAXONOMY *********/

function get_taxonomy_related_posts($post_id, $taxonomy, $args=array()) {
  $query = new WP_Query();
  $terms = wp_get_object_terms($post_id, $taxonomy);
  if (count($terms)) {
    $post_ids = get_objects_in_term($terms[0]->term_id,$taxonomy);
    $post = get_post($post_id);
    $args = wp_parse_args($args,array(
      'post_type' => $post->post_type, 
      'post__in' => $post_ids,
      'taxonomy' => $taxonomy,
      'term' => $terms[0]->slug,
	  'posts_per_page' => 3
    ));
    $query = new WP_Query($args);
  }
  return $query;
}

/********************************************/

/*************  ENABLE SESSIONS *************/

function cp_admin_init() {
	if (!session_id())
	session_start();
}

add_action('init', 'cp_admin_init');

/********************************************/


/**************  GOOGLE FONTS ***************/

function font_name($string){
		
	$check = strpos($string, ':');
	if($check == false){
		return $string;
	} else { 
		preg_match("/([\w].*):/i", $string, $matches);
		return $matches[1];
	} 
} 

/********************************************/

/************** LIST TAXONOMY ***************/

function list_taxonomy($taxonomy, $class='')
{
	$args = array ('hide_empty' => false, 'pad_counts' => 1);
	$tax_terms = get_terms($taxonomy, $args); 
	$active = '';
	$output = '<ul class="'.$class.'">';

	foreach ($tax_terms as $tax_term) {
		if ($taxonomy  == $tax_term)
		{
			$active  = ' class="active"';
		}
		$output.='<li><a href="'.esc_attr(get_term_link($tax_term, $taxonomy)) . '"'.$active.'>'.$tax_term->name.'</a></li>';
	}
	$output.='</ul>';
	
	return $output;
}

/********************************************/

add_theme_support( 'automatic-feed-links' );
if ( ! isset( $content_width ) ) $content_width = 960;
add_filter('the_excerpt', 'do_shortcode');
?>