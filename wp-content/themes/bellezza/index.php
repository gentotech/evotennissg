<?php /** The default template for pages. **/ ?>

<?php get_header(); ?>

<div class="box">
	
	<?php $promo = get_post_meta($post->ID, "_promo", $single = false);?>
	<?php if(!empty($promo[0]) ):?>
	   <div class="calloutcontainer">
			<div class="container_12">
				<div class="grid_12">            
					<?php echo do_shortcode($promo[0]);?>
				</div>
			</div>
		</div>    
	<?php endif?>
	<div class="divider_shadow"></div>

	<!--Page Content-->

	<div class="container_12">
		<div class="grid_9">
			<?php 
				$temp = $wp_query;
				$wp_query= null;
				$wp_query = new WP_Query();
				$pp = get_option('posts_per_page');
				$wp_query->query('posts_per_page='.$pp.'&paged='.$paged);			
				get_template_part('loop-default', 'index' );
				//wp_reset_postdata();
			?>		   
			</div>
			<aside class="grid_3 sidebarright alignright">
			   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?> <?php   endif;?>
			</aside> 
			<div class="clearfix"></div>
		</div>   
	</div>
	<div class="clearnospacing"></div>

<?php get_footer();?>
