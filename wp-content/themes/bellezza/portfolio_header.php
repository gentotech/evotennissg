<?php 
$al_options = get_option('al_general_settings');
$cats =  get_post_meta($post->ID, "_page_portfolio_cat", $single = true);
?>
<!-- Title -->
<div class="box pt20">
	<div class="headertext bottom0">
		<?php the_title() ?>
		<?php $headline = get_post_meta($post->ID, "_headline", $single = false);?>
		<?php if(!empty($headline[0]) ):?>
			<span><?php echo $headline[0] ?></span>
		<?php endif?>
	</div>
	<div class="clearsmall"></div>

	<!-- Promo text -->
	<?php $promo = get_post_meta($post->ID, "_promo", $single = false);?>
	<?php if(!empty($promo[0]) ):?>
	   <div class="calloutcontainer">
			<div class="container_12">
				<div class="grid_12">            
					<?php echo do_shortcode($promo[0]);?>
				</div>
			</div>
		</div>    
	<?php endif?>
	
</div>
