<?php /* Template Name: Blog (Default) */

get_header();
$catid = get_query_var('cat');
$cat = &get_category($catid);

?>

<div class="box pt0">
	<!-- Promo text -->
	<?php $promo = get_post_meta($post->ID, "_promo", $single = false);?>
	<?php if(!empty($promo[0]) ):?>
	   <div class="calloutcontainer">
			<div class="container_12">
				<div class="grid_12">            
					<?php echo do_shortcode($promo[0]);?>
				</div>
			</div>
		</div>    
	<?php endif?>

	<!-- Title -->
	<div class="headertext">
		<?php the_title() ?>
		<?php $headline = get_post_meta($post->ID, "_headline", $single = false);?>
		<?php if(!empty($headline[0]) ):?>
			<span><?php echo $headline[0] ?></span>
		<?php endif?>
	</div>
	<div class="clearnospacing"></div>

	
	<!--Page Content-->

	<div class="container_12">
		<div class="grid_9">
			<?php 
				$temp = $wp_query;
				$wp_query= null;
				$wp_query = new WP_Query();
				$pp = get_option('posts_per_page');
				$wp_query->query('posts_per_page='.$pp.'&paged='.$paged);			
				get_template_part('loop-default', 'index' );
				//wp_reset_postdata();
			?>		   
		</div>
		<aside class="grid_3_no_margin sidebarright alignright">
		   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?> <?php   endif;?>
		</aside> 
		<div class="clearfix"></div>
	</div>   
</div>
<div class="clearnospacing"></div>

<?php get_footer();?>