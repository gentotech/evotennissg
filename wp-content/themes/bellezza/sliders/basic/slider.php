<?php

$al_options = get_option('al_general_settings'); 
$loop = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC' ) );
?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#basic-slider-wrapper').bjqs({
			animation : '<?php echo $al_options['al_basic_animation']?>',
			width : <?php echo $al_options['al_sliderbasicimgw'] - 60?> ,
			height : <?php echo $al_options['al_sliderbasicimgh'] + 60?>,
			animationDuration: <?php echo $al_options['al_basic_animation_duration']?>,
			automatic: <?php echo $al_options['al_basic_autoslide']?>,
			rotationSpeed: <?php echo $al_options['al_basic_interval']?>,
			showControls: <?php echo $al_options['al_basic_controls']?>,
			nextText: '',
			prevText: '',
			showMarkers: <?php echo $al_options['al_basic_markers']?>,
			useCaptions: <?php echo $al_options['al_basic_captions']?>
        });
    });
</script>

<div class="nivo_container">
    <div id="basic-slider-wrapper">
        <ul class="bjqs">           
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <li><?php the_content() ?></li>   
            <?php endwhile ?> 
		</ul>
    </div>
</div>


<div class="clearnospacing"></div>
