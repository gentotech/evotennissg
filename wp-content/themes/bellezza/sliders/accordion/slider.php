<?php
$al_options = get_option('al_general_settings'); 
$loop = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC' ) );

global $wpdb;
$num_posts = wp_count_posts( 'slider' );
$slideCount = $num_posts->publish;
?>

<div class="container_12">
	<div class="grid_12 sticktoedge">
		<ul class="kwicks horizontal" >	
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<?php	
					$custom = get_post_custom($post->ID);
					$slideUrl = isset ($custom['_slider_link']) ? $custom['_slider_link'][0] : ''; 
				?>
				<li style="width:<?php echo floor(1040/$slideCount)?>px">					
					<div>
						<div class="kwickshadow"> </div>
						<?php if ($slideUrl !== ''):?><a href="<?php echo $slideUrl?>"><?php endif?> 
							<?php the_post_thumbnail(); ?>
						<?php if ($slideUrl !== ''):?></a><?php endif ?>						
										
						<!--
<p class="slide-minicaption">							
							<span class="slide-minicaptiontitle"><?php the_title() ?></span>						
						</p>
						<div class="slidecaption">							
							<h2 class="slidecaptiontitle">								
								<?php the_title() ?>
							</h2>							
							 <?php the_content() ?>	
						 </div>
-->											
					 </div>				
				 </li>		
				
			<?php endwhile; ?> 
		</ul>
	</div>
</div>
<div class="clearnospacing"></div>