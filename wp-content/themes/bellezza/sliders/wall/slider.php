<?php

	$al_options = get_option('al_general_settings'); 
	$loop = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC' ) );
?>

<div class="wall">
	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <?php	
            $custom = get_post_custom($post->ID);
            $link = isset ($custom['_slider_link']) ? $custom['_slider_link'][0] : ''; 
            $image_id = get_post_thumbnail_id();  
            $image_url = wp_get_attachment_image_src($image_id,'full'); 
			$thumb = wp_get_attachment_image_src($image_id,'portfolio-wall');
        ?>
		<div class="view <?php if($thumb):?>view-tenth<?php endif?>">
			<?php if(has_post_thumbnail($post->ID)) the_post_thumbnail('portfolio-wall', array('class' => 'cover')); ?>
			<div class="mask">
				<h2><?php echo get_the_title()?></h2>
				<p><?php echo get_the_content()?></p>
				<?php if ($link): ?><a href="<?php echo $link ?>" class="button round">
					<?php echo isset ($al_options['al_wall_caption']) ?  $al_options['al_wall_caption'] : __('Go!', 'Bellezza')?></a>
				<?php endif?>			
			</div>
		</div>		
	<?php endwhile; ?>   
	<div class="clearnospacing"></div>	
 </div> 
